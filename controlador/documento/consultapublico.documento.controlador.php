<?php

    require_once str_replace("\\", "/", dirname(__DIR__, 2)) . '/modelo/documento/documento.modelo.php';

    $datos = [];
    if(isset($_POST["txtNombreDocumento"])){
        $datos["nombre"] = $_POST["txtNombreDocumento"];
    }

    if(isset($_POST["txtObjeto"])){
        $datos["objeto"] = $_POST["txtObjeto"];
    }

    if(isset($_POST["txtInterviniente"])){
        $datos["interviniente"] = $_POST["txtInterviniente"];
    }

    $modeloDocumento = new ModeloDocumento();
    echo json_encode($modeloDocumento->ConsultarDocumentoAvanzado($datos));


?>