<?php
    session_start();
    require_once str_replace("\\", "/", dirname(__DIR__, 2)) . '/modelo/documento/documento.modelo.php';

    $idDocumento = $_POST["idDocumento"];
    if(filter_var($idDocumento, FILTER_VALIDATE_INT)){
        $modeloDocumento = new ModeloDocumento();
        $respuesta = $modeloDocumento->EliminarDocumento($idDocumento);
        echo json_encode($respuesta);
    }else{
        echo json_encode("El valor asignado no es numerico");
    }


?>