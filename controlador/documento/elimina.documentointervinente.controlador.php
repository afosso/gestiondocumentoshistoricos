<?php
    session_start();
    require_once str_replace("\\", "/", dirname(__DIR__, 2)) . '/modelo/documento/documentointervinente.modelo.php';

    $idDocumentoIntervinente = $_POST["idDocumentoIntervinente"];
    if(filter_var($idDocumentoIntervinente, FILTER_VALIDATE_INT)){
        $modeloDocumentoIntervinente = new ModeloDocumentoIntervinente();
        $respuesta = $modeloDocumentoIntervinente->EliminarDocumentoIntervinente($idDocumentoIntervinente);
        echo json_encode($respuesta);
    }else{
        echo json_encode("El valor asignado no es numerico");
    }


?>