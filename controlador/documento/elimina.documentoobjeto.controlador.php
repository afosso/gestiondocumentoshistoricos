<?php
    session_start();
    require_once str_replace("\\", "/", dirname(__DIR__, 2)) . '/modelo/documento/documentoobjeto.modelo.php';

    $idDocumentoObjeto = $_POST["idDocumentoObjeto"];
    if(filter_var($idDocumentoObjeto, FILTER_VALIDATE_INT)){
        $modeloDocumentoObjeto = new ModeloDocumentoObjeto();
        $respuesta = $modeloDocumentoObjeto->EliminarDocumentoObjeto($idDocumentoObjeto);
        echo json_encode($respuesta);
    }else{
        echo json_encode("El valor asignado no es numerico");
    }


?>