<?php
    session_start();
    require_once str_replace("\\", "/", dirname(__DIR__, 2)) . '/modelo/documento/entidad.modelo.php';

    $idEntidad = $_POST["idEntidad"];
    if(filter_var($idEntidad, FILTER_VALIDATE_INT)){
        $modeloEntidad = new ModeloEntidad();
        $respuesta = $modeloEntidad->EliminarEntidad($idEntidad);
        echo json_encode($respuesta);
    }else{
        echo json_encode("El valor asignado no es numerico");
    }


?>