<?php

    session_start();
    require_once str_replace("\\", "/", dirname(__DIR__, 2)) . '/modelo/documento/documentointervinente.modelo.php';
    require_once str_replace("\\", "/", dirname(__DIR__, 2)) . '/entidad/documento/documentointervinente.entidad.php';

    $respuesta = array(
        "status" => "",
        "mensaje" => ""
    );

    if(isset($_POST)){
        $documentoNombreIntervinente = new DocumentoNombreIntervinente();
        $documentoNombreIntervinente->setIdDocumento($_POST["ddlDocumento"]);
        $documentoNombreIntervinente->setNombreIntervinente($_POST["txtNombreIntervinente"]);
        $documentoNombreIntervinente->setIdUsuarioModificacion($_SESSION["idUsuario"]);
        $documentoNombreIntervinente->setIdUsuarioCreacion($_SESSION["idUsuario"]);

        if($documentoNombreIntervinente->getIdDocumento() == ""){
            $respuesta["status"] = "Error";
            $respuesta["mensaje"] = "Debe Seleccionar un documento";
            echo json_encode($respuesta);
            return;
        }

        if($documentoNombreIntervinente->getNombreIntervinente() == ""){
            $respuesta["status"] = "Error";
            $respuesta["mensaje"] = "Debe Ingresar un objeto";
            echo json_encode($respuesta);
            return;
        }

        $modeloDocumentoNombreIntervinente = new ModeloDocumentoIntervinente();
        $respuestaModelo = $modeloDocumentoNombreIntervinente->GuardarDocumentoIntervinente($documentoNombreIntervinente);
        if($respuestaModelo == "OK"){
            $respuesta["status"] = "OK";
            echo json_encode($respuesta);
        }else{
            $respuesta["status"] = "Error";
            $respuesta["mensaje"] = $respuestaModelo;
            echo json_encode($respuesta);
        }
    }

?>