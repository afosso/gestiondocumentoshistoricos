<?php

    session_start();
    require_once str_replace("\\", "/", dirname(__DIR__, 2)) . '/modelo/documento/documentoobjeto.modelo.php';
    require_once str_replace("\\", "/", dirname(__DIR__, 2)) . '/entidad/documento/documentoobjeto.entidad.php';

    $respuesta = array(
        "status" => "",
        "mensaje" => ""
    );

    if(isset($_POST)){
        $documentoObjeto = new DocumentoObjeto();
        $documentoObjeto->setIdDocumento($_POST["ddlDocumento"]);
        $documentoObjeto->setObjeto($_POST["txtObjeto"]);
        $documentoObjeto->setIdUsuarioModificacion($_SESSION["idUsuario"]);
        $documentoObjeto->setIdUsuarioCreacion($_SESSION["idUsuario"]);

        if($documentoObjeto->getIdDocumento() == ""){
            $respuesta["status"] = "Error";
            $respuesta["mensaje"] = "Debe Seleccionar un documento";
            echo json_encode($respuesta);
            return;
        }

        if($documentoObjeto->getObjeto() == ""){
            $respuesta["status"] = "Error";
            $respuesta["mensaje"] = "Debe Ingresar un objeto";
            echo json_encode($respuesta);
            return;
        }

        $modeloDocumentoObjeto = new ModeloDocumentoObjeto();
        $respuestaModelo = $modeloDocumentoObjeto->GuardarDocumentoObjeto($documentoObjeto);
        if($respuestaModelo == "OK"){
            $respuesta["status"] = "OK";
            echo json_encode($respuesta);
        }else{
            $respuesta["status"] = "Error";
            $respuesta["mensaje"] = $respuestaModelo;
            echo json_encode($respuesta);
        }
    }

?>