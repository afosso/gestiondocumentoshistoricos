<?php

    session_start();
    require_once str_replace("\\", "/", dirname(__DIR__, 2)) . '/modelo/documento/entidad.modelo.php';
    require_once str_replace("\\", "/", dirname(__DIR__, 2)) . '/entidad/documento/entidad.entidad.php';

    $respuesta = array(
        "status" => "",
        "mensaje" => ""
    );

    if(isset($_POST)){
        $entidad = new Entidad();
        $entidad->setCodigo($_POST["txtCodigo"]);
        $entidad->setDescripcion($_POST["txtDescripcion"]);
        $entidad->setEstado($_POST["ddlEstado"]);
        $entidad->setIdUsuarioModificacion($_SESSION["idUsuario"]);
        $entidad->setIdUsuarioCreacion($_SESSION["idUsuario"]);

        if($entidad->getCodigo() == ""){
            $respuesta["status"] = "Error";
            $respuesta["mensaje"] = "Debe ingresar un valor al campo código";
            echo json_encode($respuesta);
            return;
        }

        if($entidad->getDescripcion() == ""){
            $respuesta["status"] = "Error";
            $respuesta["mensaje"] = "Debe ingresar un valor al campo descripción";
            echo json_encode($respuesta);
            return;
        }

        if(!filter_var($entidad->getDescripcion(), FILTER_SANITIZE_STRING)){
            $respuesta["status"] = "Error";
            $respuesta["mensaje"] = "La descripción no debe tener carácteres especiales";
            echo json_encode($respuesta);
            return;
        }

        $modeloEntidad = new ModeloEntidad();
        $respuestaModelo = $modeloEntidad->GuardarEntidad($entidad);
        if($respuestaModelo == "OK"){
            $respuesta["status"] = "OK";
            echo json_encode($respuesta);
        }else{
            $respuesta["status"] = "Error";
            $respuesta["mensaje"] = $respuestaModelo;
            echo json_encode($respuesta);
        }
    }

?>