<?php

    session_start();
    require_once str_replace("\\", "/", dirname(__DIR__, 2)) . '/modelo/documento/documento.modelo.php';
    require_once str_replace("\\", "/", dirname(__DIR__, 2)) . '/entidad/documento/documento.entidad.php';

    $respuesta = array(
        "status" => "",
        "mensaje" => ""
    );

    if(isset($_POST)){
        $documento = new Documento();
        $documento->setIdDocumento($_POST["hiddenIdDocumento"]);
        $documento->setIdEntidad($_POST["ddlEntidad"]);
        $documento->setIdMunicipio($_POST["ddlMunicipio"]);
        $documento->setIdTipoDocumento($_POST["ddlTipoDocumento"]);
        $documento->setFecha($_POST["txtfecha"]);
        $documento->setNombreArchivo($_POST["txtNombreArchivo"]);
        $documento->setIdUsuarioModificacion($_SESSION["idUsuario"]);
        $documento->setIdUsuarioCreacion($_SESSION["idUsuario"]);

        if($documento->getIdMunicipio() == "-1"){
            $respuesta["status"] = "Error";
            $respuesta["mensaje"] = "Debe Seleccionar un municipio";
            echo json_encode($respuesta);
            return;
        }

        if($documento->getIdTipoDocumento() == "-1"){
            $respuesta["status"] = "Error";
            $respuesta["mensaje"] = "Debe seleccionar un tipo de documento";
            echo json_encode($respuesta);
            return;
        }

        if($documento->getFecha() == ""){
            $respuesta["status"] = "Error";
            $respuesta["mensaje"] = "Debe seleccionar una fecha";
            echo json_encode($respuesta);
            return;
        }

        if($documento->getNombreArchivo() == ""){
            $respuesta["status"] = "Error";
            $respuesta["mensaje"] = "Debe seleccionar un documento";
            echo json_encode($respuesta);
            return;
        }

        $modeloDocumento = new ModeloDocumento();
        $respuestaModelo = $modeloDocumento->ModificarDocumento($documento);
        if($respuestaModelo == "OK"){
            $respuesta["status"] = "OK";
            echo json_encode($respuesta);
        }else{
            $respuesta["status"] = "Error";
            $respuesta["mensaje"] = $respuestaModelo;
            echo json_encode($respuesta);
        }
    }

?>