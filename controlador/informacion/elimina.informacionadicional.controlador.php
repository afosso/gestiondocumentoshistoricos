<?php
    session_start();
    require_once str_replace("\\", "/", dirname(__DIR__, 2)) . '/modelo/informacion/informacionadicional.modelo.php';

    $idInformacionAdicional = $_POST["idInformacionAdicional"];
    if(filter_var($idInformacionAdicional, FILTER_VALIDATE_INT)){
        $modeloInformacionAdicional = new ModeloInformacionAdicional();
        $respuesta = $modeloInformacionAdicional->EliminarInformacionAdicional($idInformacionAdicional);
        echo json_encode($respuesta);
    }else{
        echo json_encode("El valor asignado no es numerico");
    }


?>