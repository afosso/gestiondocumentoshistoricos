<?php
    session_start();
    require_once str_replace("\\", "/", dirname(__DIR__, 2)) . '/modelo/informacion/informacionadicionaldocumento.modelo.php';

    $idInformacionAdicionalDocumento = $_POST["idInformacionAdicionalDocumento"];
    if(filter_var($idInformacionAdicionalDocumento, FILTER_VALIDATE_INT)){
        $modeloInformacionAdicionalDocumento = new ModeloInformacionAdicionalDocumento();
        $respuesta = $modeloInformacionAdicionalDocumento->EliminarInformacionAdicionalDocumento($idInformacionAdicionalDocumento);
        echo json_encode($respuesta);
    }else{
        echo json_encode("El valor asignado no es numerico");
    }


?>