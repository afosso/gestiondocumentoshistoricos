<?php

    session_start();
    require_once str_replace("\\", "/", dirname(__DIR__, 2)) . '/modelo/informacion/informacionadicional.modelo.php';
    require_once str_replace("\\", "/", dirname(__DIR__, 2)) . '/entidad/informacion/informacionadicional.entidad.php';

    $respuesta = array(
        "status" => "",
        "mensaje" => ""
    );

    if(isset($_POST)){
        $informacionadicional = new InformacionAdicional();
        $informacionadicional->setCodigo($_POST["txtCodigo"]);
        $informacionadicional->setDescripcion($_POST["txtDescripcion"]);
        $informacionadicional->setEstado($_POST["ddlEstado"]);
        $informacionadicional->setIdUsuarioModificacion($_SESSION["idUsuario"]);
        $informacionadicional->setIdUsuarioCreacion($_SESSION["idUsuario"]);

        if($informacionadicional->getCodigo() == ""){
            $respuesta["status"] = "Error";
            $respuesta["mensaje"] = "Debe ingresar un valor al campo código";
            echo json_encode($respuesta);
            return;
        }

        if($informacionadicional->getDescripcion() == ""){
            $respuesta["status"] = "Error";
            $respuesta["mensaje"] = "Debe ingresar un valor al campo descripción";
            echo json_encode($respuesta);
            return;
        }

        if(!filter_var($informacionadicional->getDescripcion(), FILTER_SANITIZE_STRING)){
            $respuesta["status"] = "Error";
            $respuesta["mensaje"] = "La descripción no debe tener carácteres especiales";
            echo json_encode($respuesta);
            return;
        }

        $modeloInformacionAdicional = new ModeloInformacionAdicional();
        $respuestaModelo = $modeloInformacionAdicional->GuardarInformacionAdicional($informacionadicional);
        if($respuestaModelo == "OK"){
            $respuesta["status"] = "OK";
            echo json_encode($respuesta);
        }else{
            $respuesta["status"] = "Error";
            $respuesta["mensaje"] = $respuestaModelo;
            echo json_encode($respuesta);
        }
    }

?>