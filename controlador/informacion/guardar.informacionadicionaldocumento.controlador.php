<?php

    session_start();
    require_once str_replace("\\", "/", dirname(__DIR__, 2)) . '/modelo/informacion/informacionadicionaldocumento.modelo.php';
    require_once str_replace("\\", "/", dirname(__DIR__, 2)) . '/entidad/informacion/informacionadicionaldocumento.entidad.php';

    $respuesta = array(
        "status" => "",
        "mensaje" => ""
    );

    if(isset($_POST)){
        $informacionadicionaldocumento = new InformacionAdicionalDocumento();
        $informacionadicionaldocumento->setIdInformacionAdicional($_POST["ddlInformacionAdicional"]);
        $informacionadicionaldocumento->setIdDocumento($_POST["ddlDocumento"]);
        $informacionadicionaldocumento->setValor($_POST["txtValor"]);
        $informacionadicionaldocumento->setIdUsuarioModificacion($_SESSION["idUsuario"]);
        $informacionadicionaldocumento->setIdUsuarioCreacion($_SESSION["idUsuario"]);

        if($informacionadicionaldocumento->getIdInformacionAdicional() == ""){
            $respuesta["status"] = "Error";
            $respuesta["mensaje"] = "Debe seleccionar una información adicional";
            echo json_encode($respuesta);
            return;
        }

        if($informacionadicionaldocumento->getIdDocumento() == ""){
            $respuesta["status"] = "Error";
            $respuesta["mensaje"] = "Debe seleccionar un documento";
            echo json_encode($respuesta);
            return;
        }

        if($informacionadicionaldocumento->getValor() == ""){
            $respuesta["status"] = "Error";
            $respuesta["mensaje"] = "Debe digitar un valor";
            echo json_encode($respuesta);
            return;
        }

        $modeloInformacionAdicionalDocumento = new ModeloInformacionAdicionalDocumento();
        $respuestaModelo = $modeloInformacionAdicionalDocumento->GuardarInformacionAdicionalDocumento($informacionadicionaldocumento);
        if($respuestaModelo == "OK"){
            $respuesta["status"] = "OK";
            echo json_encode($respuesta);
        }else{
            $respuesta["status"] = "Error";
            $respuesta["mensaje"] = $respuestaModelo;
            echo json_encode($respuesta);
        }
    }

?>