<?php

    session_start();
    require_once str_replace("\\", "/", dirname(__DIR__, 2)) . '/modelo/informacion/informacionadicionaldocumento.modelo.php';
    require_once str_replace("\\", "/", dirname(__DIR__, 2)) . '/entidad/informacion/informacionadicionaldocumento.entidad.php';

    $respuesta = array(
        "status" => "",
        "mensaje" => ""
    );

    if(isset($_POST)){
        $informacionAdicionalDocumento = new InformacionAdicionalDocumento();
        $informacionAdicionalDocumento->setIdInformacionAdicionalDocumento($_POST["hiddenIdInformacionAdicionalDocumento"]);
        $informacionAdicionalDocumento->setValor($_POST["txtValor"]);
        $informacionAdicionalDocumento->setIdUsuarioModificacion($_SESSION["idUsuario"]);

        if($informacionAdicionalDocumento->getValor() == ""){
            $respuesta["status"] = "Error";
            $respuesta["mensaje"] = "Debe ingresar un valor";
            echo json_encode($respuesta);
            return;
        }

        $modeloInformacionAdicionalDocumento = new ModeloInformacionAdicionalDocumento();
        $respuestaModelo = $modeloInformacionAdicionalDocumento->ModificarInformacionAdicionalDocumento($informacionAdicionalDocumento);
        if($respuestaModelo == "OK"){
            $respuesta["status"] = "OK";
            echo json_encode($respuesta);
        }else{
            $respuesta["status"] = "Error";
            $respuesta["mensaje"] = $respuestaModelo;
            echo json_encode($respuesta);
        }
    }

?>