<?php
    session_start();
    require_once str_replace("\\", "/", dirname(__DIR__, 2)) . '/modelo/seguridad/usuario.modelo.php';
    
    $modeloUsuario = new ModeloUsuario();

    if(isset($_POST) && count($_POST) > 0){
        echo json_encode($modeloUsuario->ConsultarUsuarioPorId($_POST["idUsuario"]));
    }else{
        $respuesta = $modeloUsuario->ConsultarUsuario();
        echo json_encode($respuesta);
    }
?>