<?php
    session_start();
    require_once str_replace("\\", "/", dirname(__DIR__, 2)) . '/modelo/seguridad/rol.modelo.php';

    $idRol = $_POST["idRol"];
    if(filter_var($idRol, FILTER_VALIDATE_INT)){
        $modeloRol = new ModeloRol();
        $respuesta = $modeloRol->EliminarRol($idRol);
        echo json_encode($respuesta);
    }else{
        echo json_encode("El valor asignado no es numerico");
    }


?>