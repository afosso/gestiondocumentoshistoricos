<?php
    session_start();
    require_once str_replace("\\", "/", dirname(__DIR__, 2)) . '/modelo/seguridad/usuario.modelo.php';

    $idUsuario = $_POST["idUsuario"];
    if(preg_match("/^[0-9]+$/", $idUsuario)){
        $modeloUsuario = new ModeloUsuario();
        $respuesta = $modeloUsuario->EliminarUsuario($idUsuario);
        echo json_encode($respuesta);
    }else{
        echo json_encode("El valor asignado no es numerico");
    }


?>