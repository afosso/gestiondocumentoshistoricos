<?php

    session_start();
    require_once str_replace("\\", "/", dirname(__DIR__, 2)) . '/modelo/seguridad/carpeta.modelo.php';
    require_once str_replace("\\", "/", dirname(__DIR__, 2)) . '/entidad/seguridad/carpeta.entidad.php';

    $respuesta = array(
        "status" => "",
        "mensaje" => ""
    );

    if(isset($_POST)){
        $carpeta = new Carpeta();
        $carpeta->setIdCarpetaPadre($_POST["ddlCarpetaPadre"]);
        $carpeta->setCodigo($_POST["txtCodigo"]);
        $carpeta->setDescripcion($_POST["txtDescripcion"]);
        $carpeta->setEstado($_POST["ddlEstado"]);
        $carpeta->setIdUsuarioModificacion($_SESSION["idUsuario"]);
        $carpeta->setIdUsuarioCreacion($_SESSION["idUsuario"]);

        if($carpeta->getCodigo() == ""){
            $respuesta["status"] = "Error";
            $respuesta["mensaje"] = "Debe ingresar un valor al campo código";
            echo json_encode($respuesta);
            return;
        }

        if($carpeta->getDescripcion() == ""){
            $respuesta["status"] = "Error";
            $respuesta["mensaje"] = "Debe ingresar un valor al campo descripción";
            echo json_encode($respuesta);
            return;
        }

        if(!filter_var($carpeta->getDescripcion(), FILTER_SANITIZE_STRING)){
            $respuesta["status"] = "Error";
            $respuesta["mensaje"] = "La descripción no debe tener carácteres especiales";
            echo json_encode($respuesta);
            return;
        }

        $modelocarpeta = new Modelocarpeta();
        $respuestaModelo = $modelocarpeta->GuardarCarpeta($carpeta);
        if($respuestaModelo == "OK"){
            $respuesta["status"] = "OK";
            echo json_encode($respuesta);
        }else{
            $respuesta["status"] = "Error";
            $respuesta["mensaje"] = $respuestaModelo;
            echo json_encode($respuesta);
        }
    }

?>