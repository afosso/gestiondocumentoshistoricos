<?php

    session_start();
    require_once str_replace("\\", "/", dirname(__DIR__, 2)) . '/modelo/seguridad/formulario.modelo.php';
    require_once str_replace("\\", "/", dirname(__DIR__, 2)) . '/entidad/seguridad/formulario.entidad.php';

    $respuesta = array(
        "status" => "",
        "mensaje" => ""
    );

    if(isset($_POST)){
        $formulario = new Formulario();
        $formulario->setIdFormulario($_POST["hiddenIdFormulario"]);
        $formulario->setIdCarpeta($_POST["ddlCarpeta"]);
        $formulario->setDescripcion($_POST["txtDescripcion"]);
        $formulario->setRuta($_POST["txtRuta"]);
        $formulario->setIcono($_POST["txtIcono"]);
        $formulario->setEstado($_POST["ddlEstado"]);
        $formulario->setIdUsuarioModificacion($_SESSION["idUsuario"]);
        $formulario->setIdUsuarioCreacion($_SESSION["idUsuario"]);

        if($formulario->getIdCarpeta() == "-1"){
            $respuesta["status"] = "Error";
            $respuesta["mensaje"] = "Debe seleccionar una carpeta en la cual va ubicar el formulario";
            echo json_encode($respuesta);
            return;
        }

        if($formulario->getDescripcion() == ""){
            $respuesta["status"] = "Error";
            $respuesta["mensaje"] = "Debe ingresar un valor al campo descripción";
            echo json_encode($respuesta);
            return;
        }

        if($formulario->getRuta() == ""){
            $respuesta["status"] = "Error";
            $respuesta["mensaje"] = "Debe ingresar un valor al campo ruta";
            echo json_encode($respuesta);
            return;
        }

        if($formulario->getIcono() == ""){
            $respuesta["status"] = "Error";
            $respuesta["mensaje"] = "Debe ingresar un valor al campo icono";
            echo json_encode($respuesta);
            return;
        }

        if(!filter_var($formulario->getIdCarpeta(), FILTER_VALIDATE_INT)){
            $respuesta["status"] = "Error";
            $respuesta["mensaje"] = "La carpeta no debe tener carácteres valores diferentes a entero";
            echo json_encode($respuesta);
            return;
        }

        if(!filter_var($formulario->getDescripcion(), FILTER_SANITIZE_STRING)){
            $respuesta["status"] = "Error";
            $respuesta["mensaje"] = "La descripción no debe tener carácteres especiales";
            echo json_encode($respuesta);
            return;
        }

        if(!filter_var($formulario->getRuta(), FILTER_SANITIZE_STRING)){
            $respuesta["status"] = "Error";
            $respuesta["mensaje"] = "La ruta no debe tener carácteres especiales";
            echo json_encode($respuesta);
            return;
        }

        if(!filter_var($formulario->getIcono(), FILTER_SANITIZE_STRING)){
            $respuesta["status"] = "Error";
            $respuesta["mensaje"] = "El icono no debe tener carácteres especiales";
            echo json_encode($respuesta);
            return;
        }

        $modeloFormulario = new ModeloFormulario();
        $respuestaModelo = $modeloFormulario->ModificarFormulario($formulario);
        if($respuestaModelo == "OK"){
            $respuesta["status"] = "OK";
            echo json_encode($respuesta);
        }else{
            $respuesta["status"] = "Error";
            $respuesta["mensaje"] = $respuestaModelo;
            echo json_encode($respuesta);
        }
    }

?>