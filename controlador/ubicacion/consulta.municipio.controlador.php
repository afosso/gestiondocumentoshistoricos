<?php
    require_once str_replace("\\", "/", dirname(__DIR__, 2)) . '/modelo/ubicacion/municipio.modelo.php';

    if(count($_POST) > 0){
        $modeloMunicipio = new ModeloMunicipio();
        echo json_encode($modeloMunicipio->ConsultarMunicipioPorDepartamento($_POST["idDepartamento"]));        
    }else{
        $modeloMunicipio = new ModeloMunicipio();
        echo json_encode($modeloMunicipio->ConsultarMunicipio());
    }
?>