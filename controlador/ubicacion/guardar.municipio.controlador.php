<?php

    session_start();
    require_once str_replace("\\", "/", dirname(__DIR__, 2)) . '/modelo/ubicacion/municipio.modelo.php';
    require_once str_replace("\\", "/", dirname(__DIR__, 2)) . '/entidad/ubicacion/municipio.entidad.php';

    $respuesta = array(
        "status" => "",
        "mensaje" => ""
    );

    if(isset($_POST)){
        $municipio = new Municipio();
        $municipio->setIdDepartamento($_POST["ddlDepartamento"]);
        $municipio->setCodigoDane($_POST["txtCodigoDane"]);
        $municipio->setDescripcion($_POST["txtDescripcion"]);
        $municipio->setEstado($_POST["ddlEstado"]);
        $municipio->setIdUsuarioModificacion($_SESSION["idUsuario"]);
        $municipio->setIdUsuarioCreacion($_SESSION["idUsuario"]);

        if($municipio->getIdDepartamento() == "-1"){
            $respuesta["status"] = "Error";
            $respuesta["mensaje"] = "Debe seleccionar un DEPARTAMENTO";
            echo json_encode($respuesta);
            return;
        }

        if($municipio->getCodigoDane() == ""){
            $respuesta["status"] = "Error";
            $respuesta["mensaje"] = "Debe ingresar el codigo DANE";
            echo json_encode($respuesta);
            return;
        }

        if($municipio->getDescripcion() == ""){
            $respuesta["status"] = "Error";
            $respuesta["mensaje"] = "Debe ingresar un valor al campo descripción";
            echo json_encode($respuesta);
            return;
        }

        if(!filter_var($municipio->getCodigoDane(), FILTER_SANITIZE_STRING)){
            $respuesta["status"] = "Error";
            $respuesta["mensaje"] = "El codigo DANE no debe contener valores especiales";
            echo json_encode($respuesta);
            return;
        }

        if(!filter_var($municipio->getDescripcion(), FILTER_SANITIZE_STRING)){
            $respuesta["status"] = "Error";
            $respuesta["mensaje"] = "La descripción no debe tener carácteres especiales";
            echo json_encode($respuesta);
            return;
        }

        $modeloMunicipio = new ModeloMunicipio();
        $respuestaModelo = $modeloMunicipio->GuardarMunicipio($municipio);
        if($respuestaModelo == "OK"){
            $respuesta["status"] = "OK";
            echo json_encode($respuesta);
        }else{
            $respuesta["status"] = "Error";
            $respuesta["mensaje"] = $respuestaModelo;
            echo json_encode($respuesta);
        }
    }

?>