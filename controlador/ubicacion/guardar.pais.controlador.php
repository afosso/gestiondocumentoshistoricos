<?php

    session_start();
    require_once str_replace("\\", "/", dirname(__DIR__, 2)) . '/modelo/ubicacion/pais.modelo.php';
    require_once str_replace("\\", "/", dirname(__DIR__, 2)) . '/entidad/ubicacion/pais.entidad.php';

    $respuesta = array(
        "status" => "",
        "mensaje" => ""
    );

    if(isset($_POST)){
        $pais = new Pais();
        $pais->setCodigoDane($_POST["txtCodigoDane"]);
        $pais->setDescripcion($_POST["txtDescripcion"]);
        $pais->setEstado($_POST["ddlEstado"]);
        $pais->setIdUsuarioModificacion($_SESSION["idUsuario"]);
        $pais->setIdUsuarioCreacion($_SESSION["idUsuario"]);

        if($pais->getCodigoDane() == ""){
            $respuesta["status"] = "Error";
            $respuesta["mensaje"] = "Debe ingresar el codigo DANE";
            echo json_encode($respuesta);
            return;
        }

        if($pais->getDescripcion() == ""){
            $respuesta["status"] = "Error";
            $respuesta["mensaje"] = "Debe ingresar un valor al campo descripción";
            echo json_encode($respuesta);
            return;
        }

        if(!filter_var($pais->getCodigoDane(), FILTER_SANITIZE_STRING)){
            $respuesta["status"] = "Error";
            $respuesta["mensaje"] = "El codigo DANE no debe contener valores especiales";
            echo json_encode($respuesta);
            return;
        }

        if(!filter_var($pais->getDescripcion(), FILTER_SANITIZE_STRING)){
            $respuesta["status"] = "Error";
            $respuesta["mensaje"] = "La descripción no debe tener carácteres especiales";
            echo json_encode($respuesta);
            return;
        }

        $modeloPais = new ModeloPais();
        $respuestaModelo = $modeloPais->GuardarPais($pais);
        if($respuestaModelo == "OK"){
            $respuesta["status"] = "OK";
            echo json_encode($respuesta);
        }else{
            $respuesta["status"] = "Error";
            $respuesta["mensaje"] = $respuestaModelo;
            echo json_encode($respuesta);
        }
    }

?>