<?php

    class Documento{
        private $idDocumento;
        private $idEntidad;
        private $idMunicipio;
        private $idTipoDocumento;
        private $fecha;
        private $nombreArchivo;
        private $rutaArchivo;
        private $fechaCreacion;
        private $fechaModificacion;
        private $idUsuarioCreacion;
        private $idUsuarioModificacion;
        

        /**
         * Get the value of idDocumento
         */ 
        public function getIdDocumento()
        {
                return $this->idDocumento;
        }

        /**
         * Set the value of idDocumento
         *
         * @return  self
         */ 
        public function setIdDocumento($idDocumento)
        {
                $this->idDocumento = $idDocumento;

                return $this;
        }

        /**
         * Get the value of idEntidad
         */ 
        public function getIdEntidad()
        {
                return $this->idEntidad;
        }

        /**
         * Set the value of idEntidad
         *
         * @return  self
         */ 
        public function setIdEntidad($idEntidad)
        {
                $this->idEntidad = $idEntidad;

                return $this;
        }

        /**
         * Get the value of idMunicipio
         */ 
        public function getIdMunicipio()
        {
                return $this->idMunicipio;
        }

        /**
         * Set the value of idMunicipio
         *
         * @return  self
         */ 
        public function setIdMunicipio($idMunicipio)
        {
                $this->idMunicipio = $idMunicipio;

                return $this;
        }

        /**
         * Get the value of idTipoDocumento
         */ 
        public function getIdTipoDocumento()
        {
                return $this->idTipoDocumento;
        }

        /**
         * Set the value of idTipoDocumento
         *
         * @return  self
         */ 
        public function setIdTipoDocumento($idTipoDocumento)
        {
                $this->idTipoDocumento = $idTipoDocumento;

                return $this;
        }

        /**
         * Get the value of fecha
         */ 
        public function getFecha()
        {
                return $this->fecha;
        }

        /**
         * Set the value of fecha
         *
         * @return  self
         */ 
        public function setFecha($fecha)
        {
                $this->fecha = $fecha;

                return $this;
        }

        /**
         * Get the value of nombreArchivo
         */ 
        public function getNombreArchivo()
        {
                return $this->nombreArchivo;
        }

        /**
         * Set the value of nombreArchivo
         *
         * @return  self
         */ 
        public function setNombreArchivo($nombreArchivo)
        {
                $this->nombreArchivo = $nombreArchivo;

                return $this;
        }

        /**
         * Get the value of fechaCreacion
         */ 
        public function getFechaCreacion()
        {
                return $this->fechaCreacion;
        }

        /**
         * Set the value of fechaCreacion
         *
         * @return  self
         */ 
        public function setFechaCreacion($fechaCreacion)
        {
                $this->fechaCreacion = $fechaCreacion;

                return $this;
        }

        /**
         * Get the value of fechaModificacion
         */ 
        public function getFechaModificacion()
        {
                return $this->fechaModificacion;
        }

        /**
         * Set the value of fechaModificacion
         *
         * @return  self
         */ 
        public function setFechaModificacion($fechaModificacion)
        {
                $this->fechaModificacion = $fechaModificacion;

                return $this;
        }

        /**
         * Get the value of idUsuarioCreacion
         */ 
        public function getIdUsuarioCreacion()
        {
                return $this->idUsuarioCreacion;
        }

        /**
         * Set the value of idUsuarioCreacion
         *
         * @return  self
         */ 
        public function setIdUsuarioCreacion($idUsuarioCreacion)
        {
                $this->idUsuarioCreacion = $idUsuarioCreacion;

                return $this;
        }

        /**
         * Get the value of idUsuarioModificacion
         */ 
        public function getIdUsuarioModificacion()
        {
                return $this->idUsuarioModificacion;
        }

        /**
         * Set the value of idUsuarioModificacion
         *
         * @return  self
         */ 
        public function setIdUsuarioModificacion($idUsuarioModificacion)
        {
                $this->idUsuarioModificacion = $idUsuarioModificacion;

                return $this;
        }

        /**
         * Get the value of rutaArhivo
         */ 
        public function getRutaArchivo()
        {
                return $this->rutaArchivo;
        }

        /**
         * Set the value of rutaArhivo
         *
         * @return  self
         */ 
        public function setRutaArchivo($rutaArchivo)
        {
                $this->rutaArchivo = $rutaArchivo;

                return $this;
        }
    }

?>