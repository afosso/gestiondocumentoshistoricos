<?php 

    class DocumentoIntervinente{
        private $idDocumentoIntervinente;
        private $idDocumento;
        private $nombreIntervinente;
        private $fechaCreacion;
        private $fechaModificacion;
        private $idUsuarioCreacion;
        private $idUsuarioModificacion;
        

        /**
         * Get the value of idDocumentoIntervinente
         */ 
        public function getIdDocumentoIntervinente()
        {
                return $this->idDocumentoIntervinente;
        }

        /**
         * Set the value of idDocumentoIntervinente
         *
         * @return  self
         */ 
        public function setIdDocumentoIntervinente($idDocumentoIntervinente)
        {
                $this->idDocumentoIntervinente = $idDocumentoIntervinente;

                return $this;
        }

        /**
         * Get the value of idDocumento
         */ 
        public function getIdDocumento()
        {
                return $this->idDocumento;
        }

        /**
         * Set the value of idDocumento
         *
         * @return  self
         */ 
        public function setIdDocumento($idDocumento)
        {
                $this->idDocumento = $idDocumento;

                return $this;
        }

        /**
         * Get the value of nombreIntervinente
         */ 
        public function getNombreIntervinente()
        {
                return $this->nombreIntervinente;
        }

        /**
         * Set the value of nombreIntervinente
         *
         * @return  self
         */ 
        public function setNombreIntervinente($nombreIntervinente)
        {
                $this->nombreIntervinente = $nombreIntervinente;

                return $this;
        }

        /**
         * Get the value of fechaCreacion
         */ 
        public function getFechaCreacion()
        {
                return $this->fechaCreacion;
        }

        /**
         * Set the value of fechaCreacion
         *
         * @return  self
         */ 
        public function setFechaCreacion($fechaCreacion)
        {
                $this->fechaCreacion = $fechaCreacion;

                return $this;
        }

        /**
         * Get the value of fechaModificacion
         */ 
        public function getFechaModificacion()
        {
                return $this->fechaModificacion;
        }

        /**
         * Set the value of fechaModificacion
         *
         * @return  self
         */ 
        public function setFechaModificacion($fechaModificacion)
        {
                $this->fechaModificacion = $fechaModificacion;

                return $this;
        }

        /**
         * Get the value of idUsuarioCreacion
         */ 
        public function getIdUsuarioCreacion()
        {
                return $this->idUsuarioCreacion;
        }

        /**
         * Set the value of idUsuarioCreacion
         *
         * @return  self
         */ 
        public function setIdUsuarioCreacion($idUsuarioCreacion)
        {
                $this->idUsuarioCreacion = $idUsuarioCreacion;

                return $this;
        }

        /**
         * Get the value of idUsuarioModificacion
         */ 
        public function getIdUsuarioModificacion()
        {
                return $this->idUsuarioModificacion;
        }

        /**
         * Set the value of idUsuarioModificacion
         *
         * @return  self
         */ 
        public function setIdUsuarioModificacion($idUsuarioModificacion)
        {
                $this->idUsuarioModificacion = $idUsuarioModificacion;

                return $this;
        }
    }

?>