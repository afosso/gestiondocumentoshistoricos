<?php 

    class DocumentoObjeto{
        private $idDocumentoObjeto;
        private $idDocumento;
        private $objeto;
        private $fechaCreacion;
        private $fechaModificacion;
        private $idUsuarioCreacion;
        private $idUsuarioModificacion;
        

        /**
         * Get the value of idDocumentoObjeto
         */ 
        public function getIdDocumentoObjeto()
        {
                return $this->idDocumentoObjeto;
        }

        /**
         * Set the value of idDocumentoObjeto
         *
         * @return  self
         */ 
        public function setIdDocumentoObjeto($idDocumentoObjeto)
        {
                $this->idDocumentoObjeto = $idDocumentoObjeto;

                return $this;
        }

        /**
         * Get the value of idDocumento
         */ 
        public function getIdDocumento()
        {
                return $this->idDocumento;
        }

        /**
         * Set the value of idDocumento
         *
         * @return  self
         */ 
        public function setIdDocumento($idDocumento)
        {
                $this->idDocumento = $idDocumento;

                return $this;
        }

        /**
         * Get the value of objeto
         */ 
        public function getObjeto()
        {
                return $this->objeto;
        }

        /**
         * Set the value of objeto
         *
         * @return  self
         */ 
        public function setObjeto($objeto)
        {
                $this->objeto = $objeto;

                return $this;
        }

        /**
         * Get the value of fechaCreacion
         */ 
        public function getFechaCreacion()
        {
                return $this->fechaCreacion;
        }

        /**
         * Set the value of fechaCreacion
         *
         * @return  self
         */ 
        public function setFechaCreacion($fechaCreacion)
        {
                $this->fechaCreacion = $fechaCreacion;

                return $this;
        }

        /**
         * Get the value of fechaModificacion
         */ 
        public function getFechaModificacion()
        {
                return $this->fechaModificacion;
        }

        /**
         * Set the value of fechaModificacion
         *
         * @return  self
         */ 
        public function setFechaModificacion($fechaModificacion)
        {
                $this->fechaModificacion = $fechaModificacion;

                return $this;
        }

        /**
         * Get the value of idUsuarioCreacion
         */ 
        public function getIdUsuarioCreacion()
        {
                return $this->idUsuarioCreacion;
        }

        /**
         * Set the value of idUsuarioCreacion
         *
         * @return  self
         */ 
        public function setIdUsuarioCreacion($idUsuarioCreacion)
        {
                $this->idUsuarioCreacion = $idUsuarioCreacion;

                return $this;
        }

        /**
         * Get the value of idUsuarioModificacion
         */ 
        public function getIdUsuarioModificacion()
        {
                return $this->idUsuarioModificacion;
        }

        /**
         * Set the value of idUsuarioModificacion
         *
         * @return  self
         */ 
        public function setIdUsuarioModificacion($idUsuarioModificacion)
        {
                $this->idUsuarioModificacion = $idUsuarioModificacion;

                return $this;
        }
    }

?>