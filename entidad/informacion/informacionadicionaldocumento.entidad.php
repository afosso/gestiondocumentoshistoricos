<?php

    class InformacionAdicionalDocumento{
        private $idInformacionAdicionalDocumento;
        private $idInformacionAdicional;
        private $idDocumento;
        private $valor;
        private $fechaCreacion;
        private $fechaModificacion;
        private $idUsuarioCreacion;
        private $idUsuarioModificacion;
        

        /**
         * Get the value of idInformacionAdicionalDocumento
         */ 
        public function getIdInformacionAdicionalDocumento()
        {
                return $this->idInformacionAdicionalDocumento;
        }

        /**
         * Set the value of idInformacionAdicionalDocumento
         *
         * @return  self
         */ 
        public function setIdInformacionAdicionalDocumento($idInformacionAdicionalDocumento)
        {
                $this->idInformacionAdicionalDocumento = $idInformacionAdicionalDocumento;

                return $this;
        }

        /**
         * Get the value of idInformacionAdicional
         */ 
        public function getIdInformacionAdicional()
        {
                return $this->idInformacionAdicional;
        }

        /**
         * Set the value of idInformacionAdicional
         *
         * @return  self
         */ 
        public function setIdInformacionAdicional($idInformacionAdicional)
        {
                $this->idInformacionAdicional = $idInformacionAdicional;

                return $this;
        }

        /**
         * Get the value of idDocumento
         */ 
        public function getIdDocumento()
        {
                return $this->idDocumento;
        }

        /**
         * Set the value of idDocumento
         *
         * @return  self
         */ 
        public function setIdDocumento($idDocumento)
        {
                $this->idDocumento = $idDocumento;

                return $this;
        }

        /**
         * Get the value of valor
         */ 
        public function getValor()
        {
                return $this->valor;
        }

        /**
         * Set the value of valor
         *
         * @return  self
         */ 
        public function setValor($valor)
        {
                $this->valor = $valor;

                return $this;
        }

        /**
         * Get the value of fechaCreacion
         */ 
        public function getFechaCreacion()
        {
                return $this->fechaCreacion;
        }

        /**
         * Set the value of fechaCreacion
         *
         * @return  self
         */ 
        public function setFechaCreacion($fechaCreacion)
        {
                $this->fechaCreacion = $fechaCreacion;

                return $this;
        }

        /**
         * Get the value of fechaModificacion
         */ 
        public function getFechaModificacion()
        {
                return $this->fechaModificacion;
        }

        /**
         * Set the value of fechaModificacion
         *
         * @return  self
         */ 
        public function setFechaModificacion($fechaModificacion)
        {
                $this->fechaModificacion = $fechaModificacion;

                return $this;
        }

        /**
         * Get the value of idUsuarioCreacion
         */ 
        public function getIdUsuarioCreacion()
        {
                return $this->idUsuarioCreacion;
        }

        /**
         * Set the value of idUsuarioCreacion
         *
         * @return  self
         */ 
        public function setIdUsuarioCreacion($idUsuarioCreacion)
        {
                $this->idUsuarioCreacion = $idUsuarioCreacion;

                return $this;
        }

        /**
         * Get the value of idUsuarioModificacion
         */ 
        public function getIdUsuarioModificacion()
        {
                return $this->idUsuarioModificacion;
        }

        /**
         * Set the value of idUsuarioModificacion
         *
         * @return  self
         */ 
        public function setIdUsuarioModificacion($idUsuarioModificacion)
        {
                $this->idUsuarioModificacion = $idUsuarioModificacion;

                return $this;
        }
    }

?>