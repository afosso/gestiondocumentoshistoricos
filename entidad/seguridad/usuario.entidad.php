<?php

    class Usuario{
        private $idUsuario;
        private $codigo;
        private $descripcion;
        private $password;
        private $fechaActivacion;
        private $fechaExpiracion;
        private $fotoUrl;
        private $administrador;
        private $fechaCreacion;
        private $fechaModificacion;
        private $idUsuarioCreacion;
        private $idUsuarioModificacion;

        

        /**
         * Get the value of idUsuario
         */ 
        public function getIdUsuario()
        {
                return $this->idUsuario;
        }

        /**
         * Set the value of idUsuario
         *
         * @return  self
         */ 
        public function setIdUsuario($idUsuario)
        {
                $this->idUsuario = $idUsuario;

                return $this;
        }

        /**
         * Get the value of codigo
         */ 
        public function getCodigo()
        {
                return $this->codigo;
        }

        /**
         * Set the value of codigo
         *
         * @return  self
         */ 
        public function setCodigo($codigo)
        {
                $this->codigo = $codigo;

                return $this;
        }

        /**
         * Get the value of descripcion
         */ 
        public function getDescripcion()
        {
                return $this->descripcion;
        }

        /**
         * Set the value of descripcion
         *
         * @return  self
         */ 
        public function setDescripcion($descripcion)
        {
                $this->descripcion = $descripcion;

                return $this;
        }

        /**
         * Get the value of password
         */ 
        public function getPassword()
        {
                return $this->password;
        }

        /**
         * Set the value of password
         *
         * @return  self
         */ 
        public function setPassword($password)
        {
                $this->password = $password;

                return $this;
        }

        /**
         * Get the value of fechaActivacion
         */ 
        public function getFechaActivacion()
        {
                return $this->fechaActivacion;
        }

        /**
         * Set the value of fechaActivacion
         *
         * @return  self
         */ 
        public function setFechaActivacion($fechaActivacion)
        {
                $this->fechaActivacion = $fechaActivacion;

                return $this;
        }

        /**
         * Get the value of fechaExpiracion
         */ 
        public function getFechaExpiracion()
        {
                return $this->fechaExpiracion;
        }

        /**
         * Set the value of fechaExpiracion
         *
         * @return  self
         */ 
        public function setFechaExpiracion($fechaExpiracion)
        {
                $this->fechaExpiracion = $fechaExpiracion;

                return $this;
        }

        /**
         * Get the value of fechaCreacion
         */ 
        public function getFechaCreacion()
        {
                return $this->fechaCreacion;
        }

        /**
         * Set the value of fechaCreacion
         *
         * @return  self
         */ 
        public function setFechaCreacion($fechaCreacion)
        {
                $this->fechaCreacion = $fechaCreacion;

                return $this;
        }

        /**
         * Get the value of fechaModificacion
         */ 
        public function getFechaModificacion()
        {
                return $this->fechaModificacion;
        }

        /**
         * Set the value of fechaModificacion
         *
         * @return  self
         */ 
        public function setFechaModificacion($fechaModificacion)
        {
                $this->fechaModificacion = $fechaModificacion;

                return $this;
        }

        /**
         * Get the value of idUsuarioCreacion
         */ 
        public function getIdUsuarioCreacion()
        {
                return $this->idUsuarioCreacion;
        }

        /**
         * Set the value of idUsuarioCreacion
         *
         * @return  self
         */ 
        public function setIdUsuarioCreacion($idUsuarioCreacion)
        {
                $this->idUsuarioCreacion = $idUsuarioCreacion;

                return $this;
        }

        /**
         * Get the value of idUsuarioModificacion
         */ 
        public function getIdUsuarioModificacion()
        {
                return $this->idUsuarioModificacion;
        }

        /**
         * Set the value of idUsuarioModificacion
         *
         * @return  self
         */ 
        public function setIdUsuarioModificacion($idUsuarioModificacion)
        {
                $this->idUsuarioModificacion = $idUsuarioModificacion;

                return $this;
        }

        /**
         * Get the value of fotoUrl
         */ 
        public function getFotoUrl()
        {
                return $this->fotoUrl;
        }

        /**
         * Set the value of fotoUrl
         *
         * @return  self
         */ 
        public function setFotoUrl($fotoUrl)
        {
                $this->fotoUrl = $fotoUrl;

                return $this;
        }

        /**
         * Get the value of administrador
         */ 
        public function getAdministrador()
        {
                return $this->administrador;
        }

        /**
         * Set the value of administrador
         *
         * @return  self
         */ 
        public function setAdministrador($administrador)
        {
                $this->administrador = $administrador;

                return $this;
        }
    }

?>