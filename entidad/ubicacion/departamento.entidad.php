<?php

    class Departamento{
        private $idDepartamento;
        private $idPais;
        private $codigoDane;
        private $descripcion;
        private $estado;
        private $fechaCreacion;
        private $fechaModificacion;
        private $idUsuarioCreacion;
        private $idUsuarioModificacion;

        /**
         * Get the value of idDepartamento
         */ 
        public function getIdDepartamento()
        {
                return $this->idDepartamento;
        }

        /**
         * Set the value of idDepartamento
         *
         * @return  self
         */ 
        public function setIdDepartamento($idDepartamento)
        {
                $this->idDepartamento = $idDepartamento;

                return $this;
        }

        /**
         * Get the value of idPais
         */ 
        public function getIdPais()
        {
                return $this->idPais;
        }

        /**
         * Set the value of idPais
         *
         * @return  self
         */ 
        public function setIdPais($idPais)
        {
                $this->idPais = $idPais;

                return $this;
        }

        /**
         * Get the value of codigoDane
         */ 
        public function getCodigoDane()
        {
                return $this->codigoDane;
        }

        /**
         * Set the value of codigoDane
         *
         * @return  self
         */ 
        public function setCodigoDane($codigoDane)
        {
                $this->codigoDane = $codigoDane;

                return $this;
        }

        /**
         * Get the value of descripcion
         */ 
        public function getDescripcion()
        {
                return $this->descripcion;
        }

        /**
         * Set the value of descripcion
         *
         * @return  self
         */ 
        public function setDescripcion($descripcion)
        {
                $this->descripcion = $descripcion;

                return $this;
        }

        /**
         * Get the value of estado
         */ 
        public function getEstado()
        {
                return $this->estado;
        }

        /**
         * Set the value of estado
         *
         * @return  self
         */ 
        public function setEstado($estado)
        {
                $this->estado = $estado;

                return $this;
        }

        /**
         * Get the value of fechaCreacion
         */ 
        public function getFechaCreacion()
        {
                return $this->fechaCreacion;
        }

        /**
         * Set the value of fechaCreacion
         *
         * @return  self
         */ 
        public function setFechaCreacion($fechaCreacion)
        {
                $this->fechaCreacion = $fechaCreacion;

                return $this;
        }

        /**
         * Get the value of fechaModificacion
         */ 
        public function getFechaModificacion()
        {
                return $this->fechaModificacion;
        }

        /**
         * Set the value of fechaModificacion
         *
         * @return  self
         */ 
        public function setFechaModificacion($fechaModificacion)
        {
                $this->fechaModificacion = $fechaModificacion;

                return $this;
        }

        /**
         * Get the value of idUsuarioCreacion
         */ 
        public function getIdUsuarioCreacion()
        {
                return $this->idUsuarioCreacion;
        }

        /**
         * Set the value of idUsuarioCreacion
         *
         * @return  self
         */ 
        public function setIdUsuarioCreacion($idUsuarioCreacion)
        {
                $this->idUsuarioCreacion = $idUsuarioCreacion;

                return $this;
        }

        /**
         * Get the value of idUsuarioModificacion
         */ 
        public function getIdUsuarioModificacion()
        {
                return $this->idUsuarioModificacion;
        }

        /**
         * Set the value of idUsuarioModificacion
         *
         * @return  self
         */ 
        public function setIdUsuarioModificacion($idUsuarioModificacion)
        {
                $this->idUsuarioModificacion = $idUsuarioModificacion;

                return $this;
        }
    }

?>