$(document).ready(function(){
    $.ajax({
        url: './controlador/documento/consulta.documento.controlador.php',
        type: 'post',
        dataType: 'json'
    }).done(function(response){
        var tabla = [];
        $.each(response, function(index, data){
            var botones = "<button type='button' class='btn btn-danger' onclick='Eliminar(" + data.idDocumento + ");'><i class='fa fa-trash'></i></button>";
            var obj = [
                (index+1),
                data.nombreEntidad,
                data.nombreMunicipio,
                data.nombreTipoDocumento,
                data.fecha,
                data.nombreArchivo,
                "<button type='button' class='btn btn-info' onclick='VisualizarDocumento(" + data.idDocumento + ");'><i class='fa fa-eye'></i></button>",
                botones
            ]
            tabla.push(obj);
        });
        var columnas = [
            { title : "#" },
            { title : "Entidad" },
            { title : "Municipio" },
            { title : "Tipo Documento" },
            { title : "Fecha" },
            { title : "Nombre Archivo" },
            { title : "Visualizar Archivo" },
            { title : "Acciones" }
        ]
        agregarDataTable("datos", tabla, columnas);
    }).fail(function(response){
        console.log(response.responseText);
    });
});

function Eliminar(idDocumento){
    $.ajax({
        url: './controlador/documento/elimina.documento.controlador.php',
        data: { "idDocumento" : idDocumento},
        type: 'post',
        dataType: 'json',
        success: function(response){
            if(response == "OK"){
                mostrarMensaje('succes', 'Registro eliminado con éxito');
                window.location = 'documento_documento_consulta';
            }else{
                mostrarMensaje('error', response);
            }
        }
    });
}

function EnviarModificar(idDocumento){
    window.location = "documento_documento_modifica/" + idDocumento;
}

function VisualizarDocumento(idDocumento){
    window.location = "documento_documento_visualizar/" + idDocumento;
}