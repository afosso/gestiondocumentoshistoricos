$(document).ready(function(){
    $('.consultar').click(function(event){
        event.preventDefault();
        
        var tabla = document.getElementById("datos").getElementsByTagName('tbody')[0];
        if(tabla !== undefined){
            if(tabla.rows.length > 0){
                $('#datos').dataTable().fnDestroy();
            }
        }
        
        $.ajax({
            url: './controlador/documento/consultapublico.documento.controlador.php',
            data: $('#form').serialize(),
            type: 'POST',
            dataType: 'json'
        }).done(function(response){
            var tabla = [];
        $.each(response, function(index, data){
            var obj = [
                (index+1),
                data.descripcionMunicipio,
                data.descripcionTipoDocumento,
                data.fecha,
                data.nombreArchivo,
                "<button type='button' onclick='ConsultarDocumento(" + data.idDocumento + ")' class='btn btn-warning'><i class='fa fa-eye'></i></button>"
            ]
            tabla.push(obj);
            });
            var columnas = [
                { title : "#" },
                { title : "Municipio" },
                { title : "Tipo Documento" },
                { title : "Fecha"},
                { title : "Nombre Archivo"},
                { title : "Visualizar Archivo"}
            ]
            agregarDataTable("datos", tabla, columnas);
        }).fail(function(response){
            console.log(response);
        });
    });
    
});

function ConsultarDocumento(index){
    document.location = "general_documento_visualizar/" + index;
}