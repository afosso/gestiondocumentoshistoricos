var documentos = "";
var nombres = "";

$(document).ready(function(){
    $('#txtFecha').datepicker({
        format: 'yyyy-mm-dd'
    });

    $.ajax({
        url: './controlador/documento/consulta.entidad.controlador.php',
        type: 'POST',
        dataType: 'json'
    }).done(function(response){
        var entidad = '<option value="-1">--Seleccione una opción--</option>';
        $.each(response, function(index, data){
            entidad += '<option value="' + data.idEntidad + '">' + data.descripcion + '</option>';
        });
        $('#ddlEntidad').html(entidad);
    });

    $.ajax({
        url: './controlador/tercero/consulta.tipodocumento.controlador.php',
        type: 'POST',
        dataType: 'json'
    }).done(function(response){
        var tipoDocumento = '<option value="-1">--Seleccione una opción--</option>';
        $.each(response, function(index, data){
            tipoDocumento += '<option value="' + data.idTipoDocumento + '">' + data.descripcion + '</option>';
        });
        $('#ddlTipoDocumento').html(tipoDocumento);
    });

    $.ajax({
        url: './controlador/ubicacion/consulta.departamento.controlador.php',
        type: 'POST',
        dataType: 'json'
    }).done(function(response){
        var departamento = '<option value="-1">--Seleccione una opción--</option>';
        $.each(response, function(index, data){
            departamento += '<option value="' + data.idDepartamento + '">' + data.descripcion + '</option>';
        });
        $('#ddlDepartamento').html(departamento);
    });

    $.ajax({
        url: './controlador/informacion/consulta.informacionadicional.controlador.php',
        type: 'POST',
        dataType: 'json'
    }).done(function(response){
        var informacionAdicional = '<option value="-1">--Seleccione una opción--</option>';
        $.each(response, function(index, data){
            informacionAdicional += '<option value="' + data.idInformacionAdicional + '">' + data.descripcion + '</option>';
        });
        $('#ddlInformacionAdicional').html(informacionAdicional);
    });

    $('.guardar').click(function(event){
        event.preventDefault();

        if(documentos == ""){
            mostrarMensaje('warning', "No ha seleccionado un documento");
            return;
        }
        var datos = {
            "ddlEntidad" : $('#ddlEntidad').val(),
            "ddlTipoDocumento" : $('#ddlTipoDocumento').val(),
            "ddlMunicipio" : $('#ddlMunicipio').val(),
            "txtFecha" : $('#txtFecha').val(),
            "documento" : documentos,
            "nombre" : nombres,
            "objetos" : obtenerValoresTablaObjetos(),
            "intervinente" : obtenerValoresTablaIntervinente(),
            "informacionAdicional" : obtenerValorInformacionAdicional()
        }

        $.ajax({
            url: './controlador/documento/guardar.documento.controlador.php',
            data: datos,
            type: 'POST',
            dataType: "json"
        }).done(function(response){
            if(response.status == "OK"){
                mostrarMensaje('success', 'Datos guardados con éxito');
                $('#form').trigger("reset");
            }else{
                mostrarMensaje('error', response.mensaje);
            }
        })
    });

    $('#fileDocumento').change(function(){
        var documento = this.files;
        // Trae alguna documento entonces comenzamos a recorrer para saber que no supere los 30MB
        if(documento != null && documento.length > 0){
            // validamos que el archivo tenga el tipo de documento
            if(documento[0].type != "application/pdf"){
                Swal.fire({
                    type: "error",
                    title: "Mensaje del sistema",
                    text: "El archivo " + documento[0].name + " no tiene el formato especificado (PDF)"
                });
                $('#fileDocumento').val("");
                return;
            }

            // validamos el tamaño de la documento
            if(documento[0].size > 1024000000){
                swal({
                    type: "error",
                    title: "Mensaje del sistema",
                    text: "El archivo " + documento[0].name + " pesa mas de lo permitido 1GB"
                });
                return;
            }

            var datosPDF = new FileReader;
            datosPDF.readAsDataURL(documento[0]);
            nombres = documento[0].name;

            $(datosPDF).on("load", function(event){
                documentos = event.target.result;
            });
        }
    });

    $('.botonObjetos').click(function(event){
        event.preventDefault();

        if($('#txtObjeto').val() == ""){
            mostrarMensaje("warning", "Falta llenar el campo objeto");
            return;
        }

        var tabla = document.getElementById("datosObjetos").getElementsByTagName('tbody')[0];
        var row = tabla.insertRow(tabla.rows.length);
        var cell1 = row.insertCell(0);
        var cell2 = row.insertCell(1);

        var textoCelda1 = document.createTextNode($('#txtObjeto').val());
        cell1.appendChild(textoCelda1);
        cell2.innerHTML = "<button class='btn btn-danger' type='button' onclick='EliminarFilaObjeto(" + (tabla.rows.length - 1) + ");'><i class='fa fa-trash'></i></button>";

    });

    $('.botonIntervinente').click(function(event){
        event.preventDefault();

        if($('#txtIntervinente').val() == ""){
            mostrarMensaje("warning", "Falta llenar el campo intervinente");
            return;
        }

        var tabla = document.getElementById("datosIntervinente").getElementsByTagName('tbody')[0];
        var row = tabla.insertRow(tabla.rows.length);
        var cell1 = row.insertCell(0);
        var cell2 = row.insertCell(1);

        var textoCelda1 = document.createTextNode($('#txtIntervinente').val());
        cell1.appendChild(textoCelda1);
        cell2.innerHTML = "<button class='btn btn-danger' type='button' onclick='EliminarFilaIntervinente(" + (tabla.rows.length - 1) + ");'><i class='fa fa-trash'></i></button>";

    });

    $('.botonInformacionAdicional').click(function(event){
        event.preventDefault();

        if($('#ddlInformacionAdicional').val() == "-1"){
            mostrarMensaje("warning", "Debe seleccionar la información adicional");
            return;
        }

        if($('#txtValor').val() == ""){
            mostrarMensaje("warning", "Debe ingresar un valor");
            return;
        }

        var tabla = document.getElementById("datosInformacionAdicional").getElementsByTagName('tbody')[0];
        var row = tabla.insertRow(tabla.rows.length);
        var cell1 = row.insertCell(0);
        var cell2 = row.insertCell(1);
        var cell3 = row.insertCell(2);
        var cell4 = row.insertCell(3);

        cell1.innerText = $('#ddlInformacionAdicional').val();
        cell2.innerText = $('#ddlInformacionAdicional option:selected').text();;
        cell3.innerText = $('#txtValor').val();
        cell4.innerHTML = "<button class='btn btn-danger' type='button' onclick='EliminarFilaIntervinente(" + (tabla.rows.length - 1) + ");'><i class='fa fa-trash'></i></button>";

    });
});

function EliminarFilaObjeto(index){
    var table = document.getElementById("datosObjetos").getElementsByTagName('tbody')[0];
    
    table.deleteRow(index);
}

function EliminarFilaIntervinente(index){
    var table = document.getElementById("datosIntervinente").getElementsByTagName('tbody')[0];
    
    table.deleteRow(index);
}

function ConsultaMunicipio(){
    $.ajax({
        url: './controlador/ubicacion/consulta.municipio.controlador.php',
        data: {"idDepartamento": $('#ddlDepartamento').val()},
        type: 'POST',
        dataType: 'json'
    }).done(function(response){
        var municipio = '<option value="-1">--Seleccione una opción--</option>';
        $.each(response, function(index, data){
            municipio += '<option value="' + data.idMunicipio + '">' + data.descripcion + '</option>';
        });
        $('#ddlMunicipio').html(municipio);
    });
}

function obtenerValoresTablaIntervinente(){
    var retorno = [];
    var tabla = document.getElementById("datosIntervinente").getElementsByTagName('tbody')[0];
    for (let index = 0; index < tabla.rows.length; index++) {
        var cell1 = tabla.rows.item(index).cells[0].innerText;
        retorno.push(cell1);
    }
    return retorno;
}

function obtenerValoresTablaObjetos(){
    var retorno = [];
    var tabla = document.getElementById("datosObjetos").getElementsByTagName('tbody')[0];
    for (let index = 0; index < tabla.rows.length; index++) {
        var cell1 = tabla.rows.item(index).cells[0].innerText;
        retorno.push(cell1);
    }
    return retorno;
}

function obtenerValorInformacionAdicional(){
    var retorno = [];
    var tabla = document.getElementById("datosInformacionAdicional").getElementsByTagName('tbody')[0];
    for (let index = 0; index < tabla.rows.length; index++) {
        var cell1 = tabla.rows.item(index).cells[0].innerText;
        var cell2 = tabla.rows.item(index).cells[2].innerText;
        retorno.push([
            cell1,
            cell2
        ]);
    }
    return retorno;
}