$(document).ready(function(){
    $.ajax({
        url: './controlador/documento/consulta.documentointervinente.controlador.php',
        type: 'post',
        dataType: 'json'
    }).done(function(response){
        var tabla = [];
        $.each(response, function(index, data){
            var botones = "<button type='button' class='btn btn-warning' onclick='EnviarModificar(" + data.idDocumentoIntervinente + ");'><i class='fa fa-edit'></i></button>";
            botones += "<button type='button' class='btn btn-danger' onclick='Eliminar(" + data.idDocumentoIntervinente + ");'><i class='fa fa-trash'></i></button>";
            var obj = [
                (index+1),
                data.nombreIntervinente,
                botones
            ]
            tabla.push(obj);
        });
        var columnas = [
            { title : "#" },
            { title : "Intervinente" },
            { title : "Acciones" }
        ]
        agregarDataTable("datos", tabla, columnas);
    }).fail(function(response){
        console.log(response.responseText);
    });
});

function Eliminar(idDocumentoIntervinente){
    $.ajax({
        url: './controlador/documento/elimina.documentointervinente.controlador.php',
        data: { "idDocumentoIntervinente" : idDocumentoIntervinente},
        type: 'post',
        dataType: 'json',
        success: function(response){
            if(response == "OK"){
                mostrarMensaje('succes', 'Registro eliminado con éxito');
                window.location = 'documento_documentointervinente_consulta';
            }else{
                mostrarMensaje('error', response);
            }
        }
    });
}

function EnviarModificar(idDocumentoIntervinente){
    window.location = "documento_documentointervinente_modifica/" + idDocumentoIntervinente;
}