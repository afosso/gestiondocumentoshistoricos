$(document).ready(function(){
    $.ajax({
        url: './controlador/documento/consulta.documentoobjeto.controlador.php',
        type: 'post',
        dataType: 'json'
    }).done(function(response){
        var tabla = [];
        $.each(response, function(index, data){
            var botones = "<button type='button' class='btn btn-warning' onclick='EnviarModificar(" + data.idDocumentoObjeto + ");'><i class='fa fa-edit'></i></button>";
            botones += "<button type='button' class='btn btn-danger' onclick='Eliminar(" + data.idDocumentoObjeto + ");'><i class='fa fa-trash'></i></button>";
            var obj = [
                (index+1),
                data.objeto,
                botones
            ]
            tabla.push(obj);
        });
        var columnas = [
            { title : "#" },
            { title : "Objeto" },
            { title : "Acciones" }
        ]
        agregarDataTable("datos", tabla, columnas);
    }).fail(function(response){
        console.log(response.responseText);
    });
});

function Eliminar(idDocumentoObjeto){
    $.ajax({
        url: './controlador/documento/elimina.documentoobjeto.controlador.php',
        data: { "idDocumentoObjeto" : idDocumentoObjeto},
        type: 'post',
        dataType: 'json',
        success: function(response){
            if(response == "OK"){
                mostrarMensaje('succes', 'Registro eliminado con éxito');
                window.location = 'documento_documentoobjeto_consulta';
            }else{
                mostrarMensaje('error', response);
            }
        }
    });
}

function EnviarModificar(idDocumentoObjeto){
    window.location = "documento_documentoobjeto_modifica/" + idDocumentoObjeto;
}