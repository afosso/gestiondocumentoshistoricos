$(document).ready(function(){
    $.ajax({
        url: './controlador/informacion/consulta.informacionadicional.controlador.php',
        type: 'post',
        dataType: 'json'
    }).done(function(response){
        var tabla = [];
        $.each(response, function(index, data){
            var botones = "<button type='button' class='btn btn-warning' onclick='EnviarModificar(" + data.idInformacionAdicional + ");'><i class='fa fa-edit'></i></button>";
            botones += "<button type='button' class='btn btn-danger' onclick='Eliminar(" + data.idInformacionAdicional + ");'><i class='fa fa-trash'></i></button>";
            var obj = [
                (index+1),
                data.codigo,
                data.descripcion,
                (data.estado == "1" ? "<label class='label label-success'>ACTIVO</label>" : "<label class='label label-danger'>INACTIVO</label>"),
                botones
            ]
            tabla.push(obj);
        });
        var columnas = [
            { title : "#" },
            { title : "Código" },
            { title : "Descripcion" },
            { title : "Estado"},
            { title : "Acciones" }
        ]
        agregarDataTable("datos", tabla, columnas);
    }).fail(function(response){
        console.log(response.responseText);
    });
});

function Eliminar(idInformacionAdicional){
    $.ajax({
        url: './controlador/informacion/elimina.informacionadicional.controlador.php',
        data: { "idInformacionAdicional" : idInformacionAdicional},
        type: 'post',
        dataType: 'json',
        success: function(response){
            if(response == "OK"){
                mostrarMensaje('succes', 'Registro eliminado con éxito');
                window.location = 'informacion_informacionadicional_consulta';
            }else{
                mostrarMensaje('error', response);
            }
        }
    });
}

function EnviarModificar(idInformacionAdicional){
    window.location = "informacion_informacionadicional_modifica/" + idInformacionAdicional;
}