$(document).ready(function(){
    $.ajax({
        url: './controlador/informacion/consulta.informacionadicionaldocumento.controlador.php',
        type: 'post',
        dataType: 'json'
    }).done(function(response){
        var tabla = [];
        $.each(response, function(index, data){
            var botones = "<button type='button' class='btn btn-warning' onclick='EnviarModificar(" + data.idInformacionAdicionalDocumento + ");'><i class='fa fa-edit'></i></button>";
            botones += "<button type='button' class='btn btn-danger' onclick='Eliminar(" + data.idInformacionAdicionalDocumento + ");'><i class='fa fa-trash'></i></button>";
            var obj = [
                (index+1),
                data.valor,
                botones
            ]
            tabla.push(obj);
        });
        var columnas = [
            { title : "#" },
            { title : "Código" },
            { title : "Descripcion" },
            { title : "Estado"},
            { title : "Acciones" }
        ]
        agregarDataTable("datos", tabla, columnas);
    }).fail(function(response){
        console.log(response.responseText);
    });
});

function Eliminar(idInformacionAdicionalDocumento){
    $.ajax({
        url: './controlador/informacion/elimina.informacionadicionaldocumento.controlador.php',
        data: { "idInformacionAdicionalDocumento" : idInformacionAdicionalDocumento},
        type: 'post',
        dataType: 'json',
        success: function(response){
            if(response == "OK"){
                mostrarMensaje('succes', 'Registro eliminado con éxito');
                window.location = 'informacion_informacionadicionaldocumento_consulta';
            }else{
                mostrarMensaje('error', response);
            }
        }
    });
}

function EnviarModificar(idInformacionAdicionalDocumento){
    window.location = "informacion_informacionadicionaldocumento_modifica/" + idInformacionAdicionalDocumento;
}