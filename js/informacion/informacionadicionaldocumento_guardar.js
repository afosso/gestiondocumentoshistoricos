$(document).ready(function(){
    $('.guardar').click(function(event){
        event.preventDefault();

        $.ajax({
            url: './controlador/informacion/guardar.informacionadicionaldocumento.controlador.php',
            data: $('#form').serialize(),
            type: 'POST',
            dataType: 'json'
        }).done(function(response){
            if(response.status == "OK"){
                mostrarMensaje('success', 'Datos guardados con éxito');
                $('#form').trigger("reset");
            }else{
                mostrarMensaje('error', response.mensaje);
            }
        })
    })
});