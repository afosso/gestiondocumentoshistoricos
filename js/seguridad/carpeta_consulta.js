$(document).ready(function(){
    $.ajax({
        url: './controlador/seguridad/consulta.carpeta.controlador.php',
        type: 'post',
        dataType: 'json'
    }).done(function(response){
        var tabla = [];
        $.each(response, function(index, data){
            var botones = "<button type='button' class='btn btn-warning' onclick='EnviarModificar(" + data.idCarpeta + ");'><i class='fa fa-edit'></i></button>";
            botones += "<button type='button' class='btn btn-danger' onclick='Eliminar(" + data.idCarpeta + ");'><i class='fa fa-trash'></i></button>";
            var obj = [
                (index+1),
                data.descripcionCarpetaPadre,
                data.codigo,
                data.descripcion,
                (data.estado == "1" ? "<label class='label label-success'>ACTIVO</label>" : "<label class='label label-danger'>INACTIVO</label>"),
                botones
            ]
            tabla.push(obj);
        });
        var columnas = [
            { title : "#" },
            { title : "Carpeta Padre" },
            { title : "Código" },
            { title : "Descripcion" },
            { title : "Estado"},
            { title : "Acciones" }
        ]
        agregarDataTable("datos", tabla, columnas);
    }).fail(function(response){
        console.log(response.responseText);
    });
});

function Eliminar(idCarpeta){
    $.ajax({
        url: './controlador/seguridad/elimina.carpeta.controlador.php',
        data: { "idCarpeta" : idCarpeta},
        type: 'post',
        dataType: 'json',
        success: function(response){
            if(response == "OK"){
                mostrarMensaje('succes', 'Carpeta eliminado con éxito');
                window.location = 'seguridad_carpeta_consulta';
            }else{
                mostrarMensaje('error', response);
            }
        }
    });
}

function EnviarModificar(idCarpeta){
    window.location = "seguridad_carpeta_modifica/" + idCarpeta;
}