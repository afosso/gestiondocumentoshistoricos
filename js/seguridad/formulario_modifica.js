$(document).ready(function(){
    $('.modificar').click(function(event){
        event.preventDefault();

        $.ajax({
            url: './../controlador/seguridad/modifica.formulario.controlador.php',
            data: $('#form').serialize(),
            type: 'POST',
            dataType: 'json'
        }).done(function(response){
            if(response.status == "OK"){
                mostrarMensaje('success', 'Datos modificados con éxito');
            }else{
                mostrarMensaje('error', response.mensaje);
            }
        });
    });
});