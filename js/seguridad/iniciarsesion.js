$(document).ready(function(){

    $('button').click(function(event){
        event.preventDefault();
    
        $.ajax({
            url: './controlador/seguridad/consulta.iniciarSesion.controlador.php',
            data: $('#form').serialize(),
            type: 'POST',
            success: function(response){
                if(response == "OK"){
                    window.location = 'general_inicio';
                }else{
                    Swal.fire({
                        type: 'error',
                        title: 'Mensaje del sistema',
                        text: response
                    });
                }
            }
        });
    
    });

});
