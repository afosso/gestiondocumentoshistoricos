$(document).ready(function(){
    $('.modificar').click(function(event){
        event.preventDefault();
        var data = {}
        data.hiddenIdRol = $('#hiddenIdRol').val();
        data.txtCodigo = $('#txtCodigo').val();
        data.txtDescripcion = $('#txtDescripcion').val();
        data.ddlEstado = $('#ddlEstado').val();

        $.ajax({
            url: './../controlador/seguridad/modifica.rol.controlador.php',
            data: $('#form').serialize(),
            type: 'POST',
            dataType: 'json',
            success: function(response){
                if(response.status == "OK"){
                    mostrarMensaje('success', 'Datos modificados con éxito');
                }else{
                    mostrarMensaje('error', response.mensaje);
                }
            },
            error: function(response){
                console.log(response.responseText);
            }
        });
    });
});