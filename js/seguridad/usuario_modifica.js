var imagenes;

$(document).ready(function(){

    imagenes = $('#hiddenFoto').val();

    $('#txtFechaActivacion').datepicker({
        format: 'yyyy-mm-dd'
    });

    $('#txtFechaExpiracion').datepicker({
        format: 'yyyy-mm-dd'
    });

    $('.modificar').click(function(event){
        event.preventDefault();

        var datos = {
            "NombreUsuario": $('#txtCodigo').val(),
            "DescripcionUsuario": $('#txtDescripcion').val(),
            "Contrasenia": $('#txtContrasenia').val(),
            "FechaActivacion": $('#txtFechaActivacion').val(),
            "FechaExpiracion": $('#txtFechaExpiracion').val(),
            "Administrador": document.getElementById("chkAdministrador").checked,
            "Imagen": imagenes,
            "idUsuario" : $('#hiddenIdUsuario').val()
        }

        $.ajax({
            url: './../controlador/seguridad/modifica.usuario.controlador.php',
            data: datos,
            type: 'POST',
            dataType: 'json',
            success: function(response){
                if(response.status == "OK"){
                    mostrarMensaje('success', 'Datos modificados con éxito');
                }else{
                    mostrarMensaje('error', response.mensaje);
                }
            },
            error: function(response){
                console.log(response.responseText);
            }
        });
    });

    $('#inputFotos').change(function(){
        var imagen = this.files;
        // Trae alguna imagen entonces comenzamos a recorrer para saber que no supere los 30MB
        if(imagen != null && imagen.length > 0){
            // validamos que el archivo tenga el tipo de imagen
            if(imagen[0].type != "image/jpeg" &&
            imagen[0].type != "image/jpg" &&
            imagen[0].type != "image/png"){
                Swal.fire({
                    type: "error",
                    title: "Mensaje del sistema",
                    text: "El archivo " + imagen[0].name + " no tiene el formato especificado (JPG, JPEG, PNG)"
                });
                $('#inputFotos').val("");
                return;
            }

            // validamos el tamaño de la imagen
            if(imagen[0].size > 30000000){
                swal({
                    type: "error",
                    title: "Mensaje del sistema",
                    text: "El archivo " + imagen[0].name + " pesa mas de lo permitido 30MB"
                });
                return;
            }

            var datosImagen = new FileReader;
            datosImagen.readAsDataURL(imagen[0]);

            $(datosImagen).on("load", function(event){
                imagenes = event.target.result;
                $('#imagen').attr("src", imagenes);
            });
        }
    });
});