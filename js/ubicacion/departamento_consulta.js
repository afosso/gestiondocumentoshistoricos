$(document).ready(function(){
    $.ajax({
        url: './controlador/ubicacion/consulta.departamento.controlador.php',
        type: 'post',
        dataType: 'json'
    }).done(function(response){
        var tabla = [];
        $.each(response, function(index, data){
            var botones = "<button type='button' class='btn btn-warning' onclick='EnviarModificar(" + data.idDepartamento + ");'><i class='fa fa-edit'></i></button>";
            botones += "<button type='button' class='btn btn-danger' onclick='Eliminar(" + data.idDepartamento + ");'><i class='fa fa-trash'></i></button>";
            var obj = [
                (index+1),
                data.descripcionPais,
                data.codigoDane,
                data.descripcion,
                (data.estado == "1" ? "<label class='label label-success'>ACTIVO</label>" : "<label class='label label-danger'>INACTIVO</label>"),
                botones
            ]
            tabla.push(obj);
        });
        var columnas = [
            { title : "#" },
            { title : "Pais" },
            { title : "Código DANE" },
            { title : "Descripcion" },
            { title : "Estado"},
            { title : "Acciones" }
        ]
        agregarDataTable("datos", tabla, columnas);
    }).fail(function(response){
        console.log(response.responseText);
    });
});

function Eliminar(idDepartamento){
    $.ajax({
        url: './controlador/ubicacion/elimina.departamento.controlador.php',
        data: { "idDepartamento" : idDepartamento},
        type: 'post',
        dataType: 'json',
        success: function(response){
            if(response == "OK"){
                mostrarMensaje('succes', 'Departamento eliminado con éxito');
                window.location = 'ubicacion_departamento_consulta';
            }else{
                mostrarMensaje('error', response);
            }
        }
    });
}

function EnviarModificar(idDepartamento){
    window.location = "ubicacion_departamento_modifica/" + idDepartamento;
}