$(document).ready(function(){
    $.ajax({
        url: './controlador/ubicacion/consulta.pais.controlador.php',
        type: 'post',
        dataType: 'json'
    }).done(function(response){
        var pais = '<option value="-1">--Seleccione una opción--</option>';
        $.each(response, function(index, data){
            pais += '<option value="' + data.idPais + '">' + data.descripcion + '</option>';
        });
        $('#ddlPais').html(pais);
    });

    $('.guardar').click(function(event){
        event.preventDefault();

        $.ajax({
            url: './controlador/ubicacion/guardar.departamento.controlador.php',
            data: $('#form').serialize(),
            type: 'POST',
            dataType: 'json'
        }).done(function(response){
            if(response.status == "OK"){
                mostrarMensaje('success', 'Datos guardados con éxito');
                $('#form').trigger("reset");
            }else{
                mostrarMensaje('error', response.mensaje);
            }
        })
    })
});