<?php
    
    require_once str_replace("\\", "/", dirname(__DIR__, 2)) . '/entorno/conexion.php';
    require_once str_replace("\\", "/", dirname(__DIR__, 2)) . '/entidad/documento/documento.entidad.php';

    class ModeloDocumento{
        private $conexion;

        public function ConsultarDocumento(){
            $conexion = new Conexion();
            $stmt = $conexion->prepare("SELECT
                                        d.`idDocumento`,
                                        e.`descripcion` AS 'nombreEntidad',
                                        m.`descripcion` AS 'nombreMunicipio',
                                        td.`descripcion` AS 'nombreTipoDocumento',
                                        d.`fecha`,
                                        d.`nombreArchivo`
                                    FROM `documento_documento` d
                                    LEFT JOIN `documento_entidad` e ON e.`idEntidad` = d.`idEntidad`
                                    INNER JOIN `tercero_tipodocumento` td ON td.`idTipoDocumento` = d.`idTipoDocumento`
                                    INNER JOIN `ubicacion_municipio` m ON m.`idMunicipio` = d.`idMunicipio`");
            $stmt->execute();
            return $stmt->fetchAll(PDO::FETCH_OBJ);
        }

        public function ConsultarDocumentoAvanzado($datos){
            $whereAnd = "";
            if($datos["nombre"] !== ""){
                if($whereAnd == ""){
                    $whereAnd .= " WHERE ";
                }else{
                    $whereAnd .= " AND ";
                }

                $whereAnd .= " d.nombreArchivo LIKE '%" . $datos["nombre"] . "%'";
            }

            if($datos["objeto"] !== ""){
                if($whereAnd == ""){
                    $whereAnd .= " WHERE ";
                }else{
                    $whereAnd .= " AND ";
                }

                $whereAnd .= " obj.objeto LIKE '%" . $datos["objeto"] . "%'";
            }

            if($datos["interviniente"] !== ""){
                if($whereAnd == ""){
                    $whereAnd .= " WHERE ";
                }else{
                    $whereAnd .= " AND ";
                }

                $whereAnd .= " inte.nombreIntervinente LIKE '%" . $datos["interviniente"] . "%'";
            }

            $conexion = new Conexion();
            
            $sql = "SELECT DISTINCT
                        m.`descripcion` AS  'descripcionMunicipio',
                        td.`descripcion` AS 'descripcionTipoDocumento',
                        d.`fecha`,
                        d.`nombreArchivo`,
                        d.`idDocumento`
                    FROM `documento_documento` d
                    INNER JOIN `ubicacion_municipio` m ON d.`idMunicipio` = m.`idMunicipio`
                    INNER JOIN `tercero_tipodocumento` td ON td.`idTipoDocumento` = d.`idTipoDocumento`
                    LEFT JOIN `documento_documentointervinente` inte ON inte.`idDocumento` = d.`idDocumento`
                    LEFT JOIN `documento_documentoobjeto` obj ON obj.`idDocumento` = d.`idDocumento` ";
            $sql .= $whereAnd;
            $stmt = $conexion->prepare($sql);
            $stmt->execute();
            return $stmt->fetchAll(PDO::FETCH_OBJ);
        }

        public function GuardarDocumento($documento, $objetos, $intervinente, $informacionAdicional){
            try {
                $sql = "INSERT INTO `documento_documento`
                                    (`idEntidad`,
                                    `idMunicipio`,
                                    `idTipoDocumento`,
                                    `fecha`,
                                    `nombreArchivo`,
                                    `rutaArchivo`,
                                    `fechaCreacion`,
                                    `fechaModificacion`,
                                    `idUsuarioCreacion`,
                                    `idUsuarioModificacion`)
                        VALUES (:idEntidad,
                                :idMunicipio,
                                :idTipoDocumento,
                                :fecha,
                                :nombreArchivo,
                                :rutaArchivo,
                                (SELECT NOW()),
                                (SELECT NOW()),
                                :idUsuarioCreacion,
                                :idUsuarioModificacion);";
                $conexion = new Conexion();
                $conexion->beginTransaction();
                $stmt = $conexion->prepare($sql);
                $idEntidad = $documento->getIdEntidad() == "-1" ? null : $documento->getIdEntidad();

                $stmt->bindValue(":idEntidad",$idEntidad,PDO::PARAM_INT);
                $stmt->bindValue(":idMunicipio",$documento-> getIdMunicipio(),PDO::PARAM_INT);
                $stmt->bindValue(":idTipoDocumento",$documento->getIdTipoDocumento(),PDO::PARAM_INT);
                $stmt->bindValue(":fecha",$documento->getFecha(),PDO::PARAM_STR);
                $stmt->bindValue(":nombreArchivo",$documento->getNombreArchivo(),PDO::PARAM_STR);
                $stmt->bindValue(":rutaArchivo",$documento->getRutaArchivo(),PDO::PARAM_STR);
                $stmt->bindValue(":idUsuarioCreacion",$documento->getIdUsuarioCreacion(),PDO::PARAM_STR);
                $stmt->bindValue(":idUsuarioModificacion",$documento->getIdUsuarioModificacion(),PDO::PARAM_STR);
                if($stmt->execute()){
                    $idInsertado = $conexion->lastInsertId();

                    foreach ($objetos as $key => $value) {
                        $sql = "INSERT INTO `documento_documentoobjeto`
                                        (`idDocumento`,
                                        `objeto`,
                                        `fechaCreacion`,
                                        `fechaModificacion`,
                                        `idUsuarioCreacion`,
                                        `idUsuarioModificacion`)
                            VALUES (:idDocumento,
                                    :objeto,
                                    (SELECT NOW()),
                                    (SELECT NOW()),
                                    :idUsuarioCreacion,
                                    :idUsuarioModificacion);";
                    
                        $stmt = $conexion->prepare($sql);
                        $stmt->bindValue(":idDocumento", $idInsertado, PDO::PARAM_INT);
                        $stmt->bindValue(":objeto", $value, PDO::PARAM_STR);
                        $stmt->bindValue(":idUsuarioCreacion", $documento->getIdUsuarioCreacion(), PDO::PARAM_INT);
                        $stmt->bindValue(":idUsuarioModificacion", $documento->getIdUsuarioModificacion(), PDO::PARAM_INT);   
                        if(!$stmt->execute()){
                            $conexion->rollBack();
                            return "ERROR guardado el objeto";
                        }
                    }

                    foreach ($intervinente as $key => $value) {
                        $sql = "INSERT INTO `documento_documentointervinente`
                                            (`idDocumento`,
                                            `nombreIntervinente`,
                                            `fechaCreacion`,
                                            `fechaModificacion`,
                                            `idUsuarioCreacion`,
                                            `idUsuarioModificacion`)
                                VALUES (:idDocumento,
                                        :nombreIntervinente,
                                        (SELECT NOW()),
                                        (SELECT NOW()),
                                        :idUsuarioCreacion,
                                        :idUsuarioModificacion);";
                        $stmt = $conexion->prepare($sql);
                        $stmt->bindValue(":idDocumento", $idInsertado, PDO::PARAM_INT);
                        $stmt->bindValue(":nombreIntervinente", $value, PDO::PARAM_STR);
                        $stmt->bindValue(":idUsuarioCreacion", $documento->getIdUsuarioCreacion(), PDO::PARAM_INT);
                        $stmt->bindValue(":idUsuarioModificacion", $documento->getIdUsuarioModificacion(), PDO::PARAM_INT);   
                        if(!$stmt->execute()){
                            $conexion->rollBack();
                            return "ERROR guardado el intervinente";
                        }
                    }

                    foreach ($informacionAdicional as $key => $value) {
                        $sql = "INSERT INTO `informacion_informacionadicionaldocumento`
                                            (`idInformacionAdicional`,
                                            `idDocumento`,
                                            `valor`,
                                            `fechaCreacion`,
                                            `fechaModificacion`,
                                            `idUsuarioCreacion`,
                                            `idUsuarioModificacion`)
                                VALUES (:idInformacionAdicional,
                                        :idDocumento,
                                        :valor,
                                        (SELECT NOW()),
                                        (SELECT NOW()),
                                        :idUsuarioCreacion,
                                        :idUsuarioModificacion);";
                        $stmt = $conexion->prepare($sql);
                        $stmt->bindValue(":idInformacionAdicional", $value[0], PDO::PARAM_INT);
                        $stmt->bindValue(":idDocumento", $idInsertado, PDO::PARAM_INT);
                        $stmt->bindValue(":valor", $value[1], PDO::PARAM_STR);
                        $stmt->bindValue(":idUsuarioCreacion", $documento->getIdUsuarioCreacion(), PDO::PARAM_INT);
                        $stmt->bindValue(":idUsuarioModificacion", $documento->getIdUsuarioModificacion(), PDO::PARAM_INT);
                        if(!$stmt->execute()){
                            $conexion->rollBack();
                            return "ERROR guardado el intervinente";
                        }
                    }
                    $conexion->commit();
                    return "OK";
                }
                $conexion->rollBack();
                return $stmt->errorInfo()[2];
            } catch (PDOException $error) {
                $conexion->rollBack();
                return $error->error_reporting();
            }
        }

        public function ModificarDocumento($documento){
            try {
                $sql = "UPDATE `documento_documento`
                SET `idEntidad` = :idEntidad,
                  `idMunicipio` = :idMunicipio,
                  `idTipoDocumento` = :idTipoDocumento,
                  `fecha` = :fecha,
                  `nombreArchivo` = :nombreArchivo,
                  `fechaModificacion` = (SELECT NOW()),
                  `idUsuarioModificacion` = :idUsuarioModificacion
                WHERE `idDocumento` = :idDocumento;";

                $conexion = new Conexion();
                $stmt = $conexion->prepare($sql);
                $idEntidad = $documento->getIdEntidad() == "-1" ? null : $documento->getIdEntidad();

                $stmt->bindValue(":idEntidad",$idEntidad,PDO::PARAM_INT);
                $stmt->bindValue(":idMunicipio",$documento-> getIdMunicipio(),PDO::PARAM_INT);
                $stmt->bindValue(":idTipoDocumento",$documento->getIdTipoDocumento(),PDO::PARAM_INT);
                $stmt->bindValue(":fecha",$documento->getFecha(),PDO::PARAM_STR);
                $stmt->bindValue(":nombreArchivo",$documento->getNombreArchivo(),PDO::PARAM_STR);
                $stmt->bindValue(":idDocumento",$documento->getIdDocumento(),PDO::PARAM_INT);
                $stmt->bindValue(":idUsuarioModificacion",$documento->getIdUsuarioModificacion(),PDO::PARAM_STR);
                if($stmt->execute()){
                    return "OK";
                }
                return $stmt->errorInfo()[2];

            } catch (PDOException $error) {
                return $error->error_reporting();
            }
        }

        public function EliminarDocumento($idDocumento){
            try {
                $sql = "DELETE FROM documento_documento WHERE idDocumento = :idDocumento";
                $conexion = new Conexion();
                $stmt = $conexion->prepare($sql);
                $stmt->bindValue(":idDocumento", $idDocumento, PDO::PARAM_INT);
                if($stmt->execute()){
                    return "OK";
                }
                return $stmt->errorInfo()[2];
            } catch (PDOException $error) {
                return $error->error_reporting();
            }
        }
    }

?>