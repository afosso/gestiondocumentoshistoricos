<?php
    
    require_once str_replace("\\", "/", dirname(__DIR__, 2)) . '/entorno/conexion.php';
    require_once str_replace("\\", "/", dirname(__DIR__, 2)) . '/entidad/documento/documentointervinente.entidad.php';

    class ModeloDocumentoIntervinente{
        private $conexion;

        public function ConsultarDocumentoIntervinente(){
            $conexion = new Conexion();
            $stmt = $conexion->prepare("SELECT * FROM documento_documentointervinente");
            $stmt->execute();
            return $stmt->fetchAll(PDO::FETCH_OBJ);
        }

        public function GuardarDocumentoIntervinente($documentoIntervinente){
            try {
                $sql = "INSERT INTO `documento_documentointervinente`
                                    (`idDocumento`,
                                    `nombreIntervinente`,
                                    `fechaCreacion`,
                                    `fechaModificacion`,
                                    `idUsuarioCreacion`,
                                    `idUsuarioModificacion`)
                        VALUES (:idDocumento,
                                :nombreIntervinente,
                                (SELECT NOW()),
                                (SELECT NOW()),
                                :idUsuarioCreacion,
                                :idUsuarioModificacion);";
                $conexion = new Conexion();
                $stmt = $conexion->prepare($sql);
                $stmt->bindValue(":idDocumento",$documentoIntervinente->getIdDocumento(),PDO::PARAM_INT);
                $stmt->bindValue(":nombreIntervinente",$documentoIntervinente->getNombreIntervinente(),PDO::PARAM_STR);
                $stmt->bindValue(":idUsuarioCreacion",$documentoIntervinente->getIdUsuarioCreacion(),PDO::PARAM_STR);
                $stmt->bindValue(":idUsuarioModificacion",$documentoIntervinente->getIdUsuarioModificacion(),PDO::PARAM_STR);
                if($stmt->execute()){
                    return "OK";
                }
                return $stmt->errorInfo()[2];
            } catch (PDOException $error) {
                return $error->error_reporting();
            }
        }

        public function ModificarDocumentoIntervinente($documentoIntervinente){
            try {
                $sql = "UPDATE `documento_documentointervinente`
                SET `idDocumento` = :idDocumento,
                  `nombreintervinente` = :nombreintervinente,
                  `fechaModificacion` = (SELECT NOW()),
                  `idUsuarioModificacion` = :idUsuarioModificacion
                WHERE `idDocumentoIntervinente` = :idDocumentoIntervinente;";

                $conexion = new Conexion();
                $stmt = $conexion->prepare($sql);
                $stmt->bindValue(":idDocumento",$documentoIntervinente->getIdDocumento(),PDO::PARAM_INT);
                $stmt->bindValue(":nombreintervinente",$documentoIntervinente->getNombreIntervinente(),PDO::PARAM_STR);
                $stmt->bindValue(":idDocumentoIntervinente",$documentoIntervinente->getIdDocumentoIntervinente(),PDO::PARAM_STR);
                $stmt->bindValue(":idUsuarioModificacion",$documentoIntervinente->getIdUsuarioModificacion(),PDO::PARAM_STR);
                if($stmt->execute()){
                    return "OK";
                }
                return $stmt->errorInfo()[2];

            } catch (PDOException $error) {
                return $error->error_reporting();
            }
        }

        public function EliminarDocumentoIntervinente($idDocumentoIntervinente){
            try {
                $sql = "DELETE FROM documento_documentointervinente WHERE idDocumentoIntervinente = :idDocumentoIntervinente";
                $conexion = new Conexion();
                $stmt = $conexion->prepare($sql);
                $stmt->bindValue(":idDocumentoIntervinente", $idDocumentoIntervinente, PDO::PARAM_INT);
                if($stmt->execute()){
                    return "OK";
                }
                return $stmt->errorInfo()[2];
            } catch (PDOException $error) {
                return $error->error_reporting();
            }
        }
    }

?>