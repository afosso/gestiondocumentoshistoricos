<?php
    
    require_once str_replace("\\", "/", dirname(__DIR__, 2)) . '/entorno/conexion.php';
    require_once str_replace("\\", "/", dirname(__DIR__, 2)) . '/entidad/documento/documentoobjeto.entidad.php';

    class ModeloDocumentoObjeto{
        private $conexion;

        public function ConsultarDocumentoObjeto(){
            $conexion = new Conexion();
            $stmt = $conexion->prepare("SELECT * FROM documento_documentoobjeto");
            $stmt->execute();
            return $stmt->fetchAll(PDO::FETCH_OBJ);
        }

        public function GuardarDocumentoObjeto($documentoObjeto){
            try {
                $sql = "INSERT INTO `documento_documentoobjeto`
                                    (`idDocumento`,
                                    `objeto`,
                                    `fechaCreacion`,
                                    `fechaModificacion`,
                                    `idUsuarioCreacion`,
                                    `idUsuarioModificacion`)
                        VALUES (:idDocumento,
                                :objeto,
                                (SELECT NOW()),
                                (SELECT NOW()),
                                :idUsuarioCreacion,
                                :idUsuarioModificacion);";
                $conexion = new Conexion();
                $stmt = $conexion->prepare($sql);
                $stmt->bindValue(":idDocumento",$documentoObjeto->getIdDocumento(),PDO::PARAM_INT);
                $stmt->bindValue(":objeto",$documentoObjeto->getObjeto(),PDO::PARAM_STR);
                $stmt->bindValue(":idUsuarioCreacion",$documentoObjeto->getIdUsuarioCreacion(),PDO::PARAM_STR);
                $stmt->bindValue(":idUsuarioModificacion",$documentoObjeto->getIdUsuarioModificacion(),PDO::PARAM_STR);
                if($stmt->execute()){
                    return "OK";
                }
                return $stmt->errorInfo()[2];
            } catch (PDOException $error) {
                return $error->error_reporting();
            }
        }

        public function ModificarDocumentoObjeto($documentoObjeto){
            try {
                $sql = "UPDATE `documento_documentoobjeto`
                SET `idDocumento` = :idDocumento,
                  `objeto` = :objeto,
                  `fechaModificacion` = (SELECT NOW()),
                  `idUsuarioModificacion` = :idUsuarioModificacion
                WHERE `idDocumentoObjeto` = :idDocumentoObjeto;";

                $conexion = new Conexion();
                $stmt = $conexion->prepare($sql);
                $stmt->bindValue(":idDocumento",$documentoObjeto->getIdDocumento(),PDO::PARAM_INT);
                $stmt->bindValue(":objeto",$documentoObjeto->getObjeto(),PDO::PARAM_STR);
                $stmt->bindValue(":idDocumentoObjeto",$documentoObjeto->getIdDocumentoObjeto(),PDO::PARAM_STR);
                $stmt->bindValue(":idUsuarioModificacion",$documentoObjeto->getIdUsuarioModificacion(),PDO::PARAM_STR);
                if($stmt->execute()){
                    return "OK";
                }
                return $stmt->errorInfo()[2];

            } catch (PDOException $error) {
                return $error->error_reporting();
            }
        }

        public function EliminarDocumentoObjeto($idDocumentoObjeto){
            try {
                $sql = "DELETE FROM documento_documentoobjeto WHERE idDocumentoObjeto = :idDocumentoObjeto";
                $conexion = new Conexion();
                $stmt = $conexion->prepare($sql);
                $stmt->bindValue(":idDocumentoObjeto", $idDocumentoObjeto, PDO::PARAM_INT);
                if($stmt->execute()){
                    return "OK";
                }
                return $stmt->errorInfo()[2];
            } catch (PDOException $error) {
                return $error->error_reporting();
            }
        }
    }

?>