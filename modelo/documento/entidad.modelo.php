<?php
    
    require_once str_replace("\\", "/", dirname(__DIR__, 2)) . '/entorno/conexion.php';
    require_once str_replace("\\", "/", dirname(__DIR__, 2)) . '/entidad/documento/entidad.entidad.php';

    class ModeloEntidad{
        private $conexion;

        public function ConsultarEntidad(){
            $conexion = new Conexion();
            $stmt = $conexion->prepare("SELECT * FROM documento_entidad");
            $stmt->execute();
            return $stmt->fetchAll(PDO::FETCH_OBJ);
        }

        public function GuardarEntidad($entidad){
            try {
                $sql = "INSERT INTO `documento_entidad`
                                    (`codigo`,
                                    `descripcion`,
                                    `estado`,
                                    `fechaCreacion`,
                                    `fechaModificacion`,
                                    `idUsuarioCreacion`,
                                    `idUsuarioModificacion`)
                        VALUES (:codigo,
                                :descripcion,
                                :estado,
                                (SELECT NOW()),
                                (SELECT NOW()),
                                :idUsuarioCreacion,
                                :idUsuarioModificacion);";
                $conexion = new Conexion();
                $stmt = $conexion->prepare($sql);
                $stmt->bindValue(":codigo",$entidad->getCodigo(),PDO::PARAM_STR);
                $stmt->bindValue(":descripcion",$entidad->getDescripcion(),PDO::PARAM_STR);
                $stmt->bindValue(":estado",$entidad->getEstado(),PDO::PARAM_INT);
                $stmt->bindValue(":idUsuarioCreacion",$entidad->getIdUsuarioCreacion(),PDO::PARAM_STR);
                $stmt->bindValue(":idUsuarioModificacion",$entidad->getIdUsuarioModificacion(),PDO::PARAM_STR);
                if($stmt->execute()){
                    return "OK";
                }
                return $stmt->errorInfo()[2];
            } catch (PDOException $error) {
                return $error->error_reporting();
            }
        }

        public function ModificarEntidad($entidad){
            try {
                $sql = "UPDATE `documento_entidad`
                SET `codigo` = :codigo,
                  `descripcion` = :descripcion,
                  `estado` = :estado,
                  `fechaModificacion` = (SELECT NOW()),
                  `idUsuarioModificacion` = :idUsuarioModificacion
                WHERE `idEntidad` = :idEntidad;";

                $conexion = new Conexion();
                $stmt = $conexion->prepare($sql);
                $stmt->bindValue(":codigo",$entidad->getCodigo(),PDO::PARAM_STR);
                $stmt->bindValue(":descripcion",$entidad->getDescripcion(),PDO::PARAM_STR);
                $stmt->bindValue(":estado",$entidad->getEstado(),PDO::PARAM_INT);
                $stmt->bindValue(":idEntidad",$entidad->getIdEntidad(),PDO::PARAM_STR);
                $stmt->bindValue(":idUsuarioModificacion",$entidad->getIdUsuarioModificacion(),PDO::PARAM_STR);
                if($stmt->execute()){
                    return "OK";
                }
                return $stmt->errorInfo()[2];

            } catch (PDOException $error) {
                return $error->error_reporting();
            }
        }

        public function EliminarEntidad($idEntidad){
            try {
                $sql = "DELETE FROM documento_entidad WHERE idEntidad = :idEntidad";
                $conexion = new Conexion();
                $stmt = $conexion->prepare($sql);
                $stmt->bindValue(":idEntidad", $idEntidad, PDO::PARAM_INT);
                if($stmt->execute()){
                    return "OK";
                }
                return $stmt->errorInfo()[2];
            } catch (PDOException $error) {
                return $error->error_reporting();
            }
        }
    }

?>