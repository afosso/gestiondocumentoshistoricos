<?php
    
    require_once str_replace("\\", "/", dirname(__DIR__, 2)) . '/entorno/conexion.php';
    require_once str_replace("\\", "/", dirname(__DIR__, 2)) . '/entidad/informacion/informacionadicional.entidad.php';

    class ModeloInformacionAdicional{
        private $conexion;

        public function ConsultarInformacionAdicional(){
            $conexion = new Conexion();
            $stmt = $conexion->prepare("SELECT * FROM informacion_informacionadicional");
            $stmt->execute();
            return $stmt->fetchAll(PDO::FETCH_OBJ);
        }

        public function GuardarInformacionAdicional($informacionAdicional){
            try {
                $sql = "INSERT INTO `informacion_informacionadicional`
                                    (`codigo`,
                                    `descripcion`,
                                    `estado`,
                                    `fechaCreacion`,
                                    `fechaModificacion`,
                                    `idUsuarioCreacion`,
                                    `idUsuarioModificacion`)
                        VALUES (:codigo,
                                :descripcion,
                                :estado,
                                (SELECT NOW()),
                                (SELECT NOW()),
                                :idUsuarioCreacion,
                                :idUsuarioModificacion);";
                $conexion = new Conexion();
                $stmt = $conexion->prepare($sql);
                $stmt->bindValue(":codigo",$informacionAdicional->getCodigo(),PDO::PARAM_STR);
                $stmt->bindValue(":descripcion",$informacionAdicional->getDescripcion(),PDO::PARAM_STR);
                $stmt->bindValue(":estado",$informacionAdicional->getEstado(),PDO::PARAM_INT);
                $stmt->bindValue(":idUsuarioCreacion",$informacionAdicional->getIdUsuarioCreacion(),PDO::PARAM_STR);
                $stmt->bindValue(":idUsuarioModificacion",$informacionAdicional->getIdUsuarioModificacion(),PDO::PARAM_STR);
                if($stmt->execute()){
                    return "OK";
                }
                return $stmt->errorInfo()[2];
            } catch (PDOException $error) {
                return $error->error_reporting();
            }
        }

        public function ModificarInformacionAdicional($informacionAdicional){
            try {
                $sql = "UPDATE `informacion_informacionadicional`
                SET `codigo` = :codigo,
                  `descripcion` = :descripcion,
                  `estado` = :estado,
                  `fechaModificacion` = (SELECT NOW()),
                  `idUsuarioModificacion` = :idUsuarioModificacion
                WHERE `idInformacionAdicional` = :idInformacionAdicional;";

                $conexion = new Conexion();
                $stmt = $conexion->prepare($sql);
                $stmt->bindValue(":codigo",$informacionAdicional->getCodigo(),PDO::PARAM_STR);
                $stmt->bindValue(":descripcion",$informacionAdicional->getDescripcion(),PDO::PARAM_STR);
                $stmt->bindValue(":estado",$informacionAdicional->getEstado(),PDO::PARAM_INT);
                $stmt->bindValue(":idInformacionAdicional",$informacionAdicional->getIdInformacionAdicional(),PDO::PARAM_STR);
                $stmt->bindValue(":idUsuarioModificacion",$informacionAdicional->getIdUsuarioModificacion(),PDO::PARAM_STR);
                if($stmt->execute()){
                    return "OK";
                }
                return $stmt->errorInfo()[2];

            } catch (PDOException $error) {
                return $error->error_reporting();
            }
        }

        public function EliminarInformacionAdicional($idInformacionAdicional){
            try {
                $sql = "DELETE FROM informacion_informacionadicional WHERE idInformacionAdicional = :idInformacionAdicional";
                $conexion = new Conexion();
                $stmt = $conexion->prepare($sql);
                $stmt->bindValue(":idInformacionAdicional", $idInformacionAdicional, PDO::PARAM_INT);
                if($stmt->execute()){
                    return "OK";
                }
                return $stmt->errorInfo()[2];
            } catch (PDOException $error) {
                return $error->error_reporting();
            }
        }
    }

?>