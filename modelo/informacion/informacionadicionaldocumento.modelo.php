<?php
    
    require_once str_replace("\\", "/", dirname(__DIR__, 2)) . '/entorno/conexion.php';
    require_once str_replace("\\", "/", dirname(__DIR__, 2)) . '/entidad/informacion/informacionadicionaldocumento.entidad.php';

    class ModeloInformacionAdicionalDocumento{
        private $conexion;

        public function ConsultarInformacionAdicionalDocumento(){
            $conexion = new Conexion();
            $stmt = $conexion->prepare("SELECT * FROM informacion_informacionadicionaldocumento");
            $stmt->execute();
            return $stmt->fetchAll(PDO::FETCH_OBJ);
        }

        public function GuardarInformacionAdicionalDocumento($informacionAdicionalDocumento){
            try {
                $sql = "INSERT INTO `informacion_informacionadicionaldocumento`
                                    (`idInformacionAdicional`,
                                    `idDocumento`,
                                    `valor`,
                                    `fechaCreacion`,
                                    `fechaModificacion`,
                                    `idUsuarioCreacion`,
                                    `idUsuarioModificacion`)
                        VALUES (:idInformacionAdicional,
                                :idDocumento,
                                :valor,
                                (SELECT NOW()),
                                (SELECT NOW()),
                                :idUsuarioCreacion,
                                :idUsuarioModificacion);";
                $conexion = new Conexion();
                $stmt = $conexion->prepare($sql);
                $stmt->bindValue(":idInformacionAdicional",$informacionAdicionalDocumento->getIdInformacionAdicional(),PDO::PARAM_STR);
                $stmt->bindValue(":idDocumento",$informacionAdicionalDocumento->getIdDocumento(),PDO::PARAM_STR);
                $stmt->bindValue(":valor",$informacionAdicionalDocumento->getValor(),PDO::PARAM_INT);
                $stmt->bindValue(":idUsuarioCreacion",$informacionAdicionalDocumento->getIdUsuarioCreacion(),PDO::PARAM_STR);
                $stmt->bindValue(":idUsuarioModificacion",$informacionAdicionalDocumento->getIdUsuarioModificacion(),PDO::PARAM_STR);
                if($stmt->execute()){
                    return "OK";
                }
                return $stmt->errorInfo()[2];
            } catch (PDOException $error) {
                return $error->error_reporting();
            }
        }

        public function ModificarInformacionAdicionalDocumento($informacionAdicionalDocumento){
            try {
                $sql = "UPDATE `informacion_informacionadicionaldocumento`
                SET `valor` = :valor,
                  `fechaModificacion` = (SELECT NOW()),
                  `idUsuarioModificacion` = :idUsuarioModificacion
                WHERE `idInformacionAdicionalDocumento` = :idInformacionAdicionalDocumento;";

                $conexion = new Conexion();
                $stmt = $conexion->prepare($sql);
                $stmt->bindValue(":valor",$informacionAdicionalDocumento->getValor(),PDO::PARAM_STR);
                $stmt->bindValue(":idInformacionAdicionalDocumento",$informacionAdicionalDocumento->getIdInformacionAdicionalDocumento(),PDO::PARAM_STR);
                $stmt->bindValue(":idUsuarioModificacion",$informacionAdicionalDocumento->getIdUsuarioModificacion(),PDO::PARAM_STR);
                if($stmt->execute()){
                    return "OK";
                }
                return $stmt->errorInfo()[2];

            } catch (PDOException $error) {
                return $error->error_reporting();
            }
        }

        public function EliminarInformacionAdicionalDocumento($idInformacionAdicionalDocumento){
            try {
                $sql = "DELETE FROM informacion_informacionadicionaldocumento WHERE idInformacionAdicionalDocumento = :idInformacionAdicionalDocumento";
                $conexion = new Conexion();
                $stmt = $conexion->prepare($sql);
                $stmt->bindValue(":idInformacionAdicionalDocumento", $idInformacionAdicionalDocumento, PDO::PARAM_INT);
                if($stmt->execute()){
                    return "OK";
                }
                return $stmt->errorInfo()[2];
            } catch (PDOException $error) {
                return $error->error_reporting();
            }
        }
    }

?>