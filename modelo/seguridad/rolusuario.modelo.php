<?php

    require_once str_replace("\\", "/", dirname(__DIR__, 2)) . '/entorno/conexion.php';
    require_once str_replace("\\", "/", dirname(__DIR__, 2)) . '/entidad/seguridad/rolusuario.entidad.php';

    class ModeloRolUsuario{
        private $conexion;

        public function ConsultarRolUsuarioPorIdUsuario($idUsuario){
            $conexion = new Conexion();
            $stmt = $conexion->prepare("SELECT * FROM seguridad_rolusuario WHERE idUsuario = :idUsuario");
            $stmt->bindValue(":idUsuario", $idUsuario, PDO::PARAM_INT);
            $stmt->execute();
            return $stmt->fetchAll(PDO::FETCH_OBJ);
        }

        public function GuardarRolUsuario($rolUsuario){
            try {
                $sql = "INSERT INTO `seguridad_rolusuario`
                                    (`idRol`,
                                    `idUsuario`)
                        VALUES (:idRol,
                                :idUsuario);";
                $conexion = new Conexion();
                $stmt = $conexion->prepare($sql);
                $stmt->bindValue(":idRol", $rolUsuario->getIdRol(), PDO::PARAM_INT);
                $stmt->bindValue(":idUsuario", $rolUsuario->getIdUsuario(), PDO::PARAM_INT);
                if($stmt->execute()){
                    return "OK";
                }
                return $stmt->errorInfo()[2];
            } catch (PDOException $error) {
                return $error->error_reporting();
            }
        }

        public function EliminarRolUsuario($rolUsuario){
            try {
                $conexion = new Conexion();
                $stmt = $conexion->prepare("DELETE FROM seguridad_rolusuario WHERE idRol = :idRol AND idUsuario = :idUsuario");
                $stmt->bindValue(":idRol", $rolUsuario->getIdRol(), PDO::PARAM_INT);
                $stmt->bindValue(":idUsuario", $rolUsuario->getIdUsuario(), PDO::PARAM_INT);
                if($stmt->execute()){
                    return "OK";
                }
                return $stmt->errorInfo()[2];
            } catch (PDOException $error) {
                return $error->error_reporting();
            }
        }
    }

?>