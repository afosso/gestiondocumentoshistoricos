<?php

    require_once str_replace("\\", "/", dirname(__DIR__, 2)) . '/entorno/conexion.php';
    require_once str_replace("\\", "/", dirname(__DIR__, 2)) . '/entidad/seguridad/usuario.entidad.php';

    class ModeloUsuario{
        private $conexion;

        public function IniciarSesion($usuario){
            $sql = "SELECT * FROM seguridad_usuario WHERE codigo = :codigo";
            $conexion = new Conexion();
            $stmt = $conexion->prepare($sql);
            $stmt->bindValue(":codigo", $usuario, PDO::PARAM_STR);
            $stmt->execute();
            return $stmt->fetch(PDO::FETCH_OBJ);
        }

        public function ConsultarUsuario(){
            $sql = "SELECT * FROM seguridad_usuario";
            $conexion = new Conexion();
            $stmt = $conexion->prepare($sql);
            $stmt->execute();
            return $stmt->fetchAll(\PDO::FETCH_OBJ);
        }

        public function ConsultarUsuarioPorId($idUsuario){
            $sql = "SELECT * FROM seguridad_usuario WHERE idUsuario = :idUsuario";
            $conexion = new Conexion();
            $stmt = $conexion->prepare($sql);
            $stmt->execute();
            return $stmt->fetch(\PDO::FETCH_OBJ);
        }

        public function AgregarUsuario($usuario){
            try{
                $sql = "INSERT INTO `seguridad_usuario`
                                    (`codigo`,`descripcion`,`password`,`fechaActivacion`,`fechaExpiracion`,`fotoUrl`,`administrador`,`fechaCreacion`,`fechaModificacion`,`idUsuarioCreacion`,`idUsuarioModificacion`)
                        VALUES (:codigo,:descripcion,:PASSWORD,:fechaActivacion,:fechaExpiracion,:fotoUrl,:administrador,(SELECT NOW()),(SELECT NOW()),:idUsuarioCreacion,:idUsuarioModificacion);";
                $conexion = new Conexion();
                $stmt = $conexion->prepare($sql);
                $stmt->bindValue(":codigo", $usuario->getCodigo(), PDO::PARAM_STR);
                $stmt->bindValue(":descripcion", $usuario->getDescripcion(), PDO::PARAM_STR);
                $stmt->bindValue(":PASSWORD", $usuario->getPassword(), PDO::PARAM_STR);
                $stmt->bindValue(":fechaActivacion", $usuario->getFechaActivacion(), PDO::PARAM_STR);
                $stmt->bindValue(":fechaExpiracion", $usuario->getFechaExpiracion(), PDO::PARAM_STR);
                $stmt->bindValue(":fotoUrl", $usuario->getFotoUrl(), PDO::PARAM_STR);
                $stmt->bindValue(":administrador", $usuario->getAdministrador(), PDO::PARAM_INT);
                $stmt->bindValue(":idUsuarioCreacion", $usuario->getIdUsuarioCreacion(), PDO::PARAM_INT);
                $stmt->bindValue(":idUsuarioModificacion", $usuario->getIdUsuarioModificacion(), PDO::PARAM_INT);
                if($stmt->execute()){
                    return "OK";
                }
                return $stmt->errorInfo()[2];
            }catch(PDOException $error){
                return $error;
            }
        }

        public function ModificarUsuario($usuario){
            try{
                $sql = "UPDATE `seguridad_usuario`
                SET `codigo` = :codigo,
                  `descripcion` = :descripcion,
                  " . ($usuario->getPassword() != "" ? "`password` = '" . $usuario->getPassword() . "'," : "" ) . "
                  `fechaActivacion` = :fechaActivacion,
                  `fechaExpiracion` = :fechaExpiracion,
                  `fotoUrl` = :fotoUrl,
                  `administrador` = :administrador,
                  `fechaModificacion` = (SELECT NOW()),
                  `idUsuarioModificacion` = :idUsuarioModificacion
                WHERE `idUsuario` = :idUsuario;";

                
                $conexion = new Conexion();
                $stmt = $conexion->prepare($sql);
                $stmt->bindValue(":codigo", $usuario->getCodigo(), PDO::PARAM_STR);
                $stmt->bindValue(":descripcion", $usuario->getDescripcion(), PDO::PARAM_STR);
                $stmt->bindValue(":fechaActivacion", $usuario->getFechaActivacion(), PDO::PARAM_STR);
                $stmt->bindValue(":fechaExpiracion", $usuario->getFechaExpiracion(), PDO::PARAM_STR);
                $stmt->bindValue(":fotoUrl", $usuario->getFotoUrl(), PDO::PARAM_LOB);
                $stmt->bindValue(":administrador", $usuario->getAdministrador(), PDO::PARAM_INT);
                $stmt->bindValue(":idUsuario", $usuario->getIdUsuario(), PDO::PARAM_INT);
                $stmt->bindValue(":idUsuarioModificacion", $usuario->getIdUsuarioModificacion(), PDO::PARAM_INT);
                if($stmt->execute()){
                    return "OK";
                }
                return $stmt->errorInfo()[2];
            }catch(PDOException $error){
                return $error;
            }
        }

        public function EliminarUsuario($idUsuario){
            try{
                $sql = "DELETE FROM seguridad_usuario WHERE idUsuario = :idUsuario";
                $conexion = new Conexion();
                $stmt = $conexion->prepare($sql);
                $stmt->bindParam(":idUsuario", $idUsuario, PDO::PARAM_INT);
                if($stmt->execute()){
                    return "OK";
                }
                return $stmt->errorInfo()[2];
            }catch(PDOException $error){
                return $error;
            }
        }

        

    }

?>