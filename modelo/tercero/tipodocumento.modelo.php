<?php
    
    require_once str_replace("\\", "/", dirname(__DIR__, 2)) . '/entorno/conexion.php';
    require_once str_replace("\\", "/", dirname(__DIR__, 2)) . '/entidad/tercero/tipodocumento.entidad.php';

    class ModeloTipoDocumento{
        private $conexion;

        public function ConsultarTipoDocumento(){
            $conexion = new Conexion();
            $stmt = $conexion->prepare("SELECT * FROM tercero_tipodocumento");
            $stmt->execute();
            return $stmt->fetchAll(PDO::FETCH_OBJ);
        }

        public function GuardarTipoDocumento($tipoDocumento){
            try {
                $sql = "INSERT INTO `tercero_tipodocumento`
                                    (`codigo`,
                                    `descripcion`,
                                    `estado`,
                                    `fechaCreacion`,
                                    `fechaModificacion`,
                                    `idUsuarioCreacion`,
                                    `idUsuarioModificacion`)
                        VALUES (:codigo,
                                :descripcion,
                                :estado,
                                (SELECT NOW()),
                                (SELECT NOW()),
                                :idUsuarioCreacion,
                                :idUsuarioModificacion);";
                $conexion = new Conexion();
                $stmt = $conexion->prepare($sql);
                $stmt->bindValue(":codigo",$tipoDocumento->getCodigo(),PDO::PARAM_STR);
                $stmt->bindValue(":descripcion",$tipoDocumento->getDescripcion(),PDO::PARAM_STR);
                $stmt->bindValue(":estado",$tipoDocumento->getEstado(),PDO::PARAM_INT);
                $stmt->bindValue(":idUsuarioCreacion",$tipoDocumento->getIdUsuarioCreacion(),PDO::PARAM_STR);
                $stmt->bindValue(":idUsuarioModificacion",$tipoDocumento->getIdUsuarioModificacion(),PDO::PARAM_STR);
                if($stmt->execute()){
                    return "OK";
                }
                return $stmt->errorInfo()[2];
            } catch (PDOException $error) {
                return $error->error_reporting();
            }
        }

        public function ModificarTipoDocumento($tipoDocumento){
            try {
                $sql = "UPDATE `tercero_tipodocumento`
                SET `codigo` = :codigo,
                  `descripcion` = :descripcion,
                  `estado` = :estado,
                  `fechaModificacion` = (SELECT NOW()),
                  `idUsuarioModificacion` = :idUsuarioModificacion
                WHERE `idTipoDocumento` = :idTipoDocumento;";

                $conexion = new Conexion();
                $stmt = $conexion->prepare($sql);
                $stmt->bindValue(":codigo",$tipoDocumento->getCodigo(),PDO::PARAM_STR);
                $stmt->bindValue(":descripcion",$tipoDocumento->getDescripcion(),PDO::PARAM_STR);
                $stmt->bindValue(":estado",$tipoDocumento->getEstado(),PDO::PARAM_INT);
                $stmt->bindValue(":idTipoDocumento",$tipoDocumento->getIdTipoDocumento(),PDO::PARAM_STR);
                $stmt->bindValue(":idUsuarioModificacion",$tipoDocumento->getIdUsuarioModificacion(),PDO::PARAM_STR);
                if($stmt->execute()){
                    return "OK";
                }
                return $stmt->errorInfo()[2];

            } catch (PDOException $error) {
                return $error->error_reporting();
            }
        }

        public function EliminarTipoDocumento($idTipoDocumento){
            try {
                $sql = "DELETE FROM tercero_tipodocumento WHERE idTipoDocumento = :idTipoDocumento";
                $conexion = new Conexion();
                $stmt = $conexion->prepare($sql);
                $stmt->bindValue(":idTipoDocumento", $idTipoDocumento, PDO::PARAM_INT);
                if($stmt->execute()){
                    return "OK";
                }
                return $stmt->errorInfo()[2];
            } catch (PDOException $error) {
                return $error->error_reporting();
            }
        }
    }

?>