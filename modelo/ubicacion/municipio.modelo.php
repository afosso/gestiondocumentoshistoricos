<?php

    require_once str_replace("\\", "/", dirname(__DIR__, 2)) . '/entorno/conexion.php';
    require_once str_replace("\\", "/", dirname(__DIR__, 2)) . '/entidad/ubicacion/municipio.entidad.php';

    class ModeloMunicipio{
        private $conexion;

        public function ConsultarMunicipio(){
            $conexion = new Conexion();
            $stmt = $conexion->prepare("SELECT m.*, d.descripcion AS 'descripcionDepartamento' FROM ubicacion_municipio m INNER JOIN ubicacion_departamento d ON d.idDepartamento = m.idDepartamento");
            $stmt->execute();
            return $stmt->fetchAll(PDO::FETCH_OBJ);
        }

        public function ConsultarMunicipioPorDepartamento($idDepartamento){
            $conexion = new Conexion();
            $stmt = $conexion->prepare("SELECT * FROM ubicacion_municipio WHERE idDepartamento = :idDepartamento");
            $stmt->bindValue(":idDepartamento", $idDepartamento, PDO::PARAM_INT);
            $stmt->execute();
            return $stmt->fetchAll(PDO::FETCH_OBJ);
        }

        public function GuardarMunicipio($municipio){
            try {
                $sql = "INSERT INTO `ubicacion_municipio`
                                    (`idDepartamento`,
                                    `codigoDane`,
                                    `descripcion`,
                                    `estado`,
                                    `fechaCreacion`,
                                    `fechaModificacion`,
                                    `idUsuarioCreacion`,
                                    `idUsuarioModificacion`)
                        VALUES (:idDepartamento,
                                :codigoDane,
                                :descripcion,
                                :estado,
                                (SELECT NOW()),
                                (SELECT NOW()),
                                :idUsuarioCreacion,
                                :idUsuarioModificacion);";

                $conexion = new Conexion();
                $stmt = $conexion->prepare($sql);

                $stmt->bindValue(":idDepartamento", $municipio->getIdDepartamento(), PDO::PARAM_STR);
                $stmt->bindValue(":codigoDane", $municipio->getCodigoDane(), PDO::PARAM_STR);
                $stmt->bindValue(":descripcion", $municipio->getDescripcion(), PDO::PARAM_STR);
                $stmt->bindValue(":estado", $municipio->getEstado(), PDO::PARAM_INT);
                $stmt->bindValue(":idUsuarioCreacion", $municipio->getIdUsuarioCreacion(), PDO::PARAM_STR);
                $stmt->bindValue(":idUsuarioModificacion", $municipio->getIdUsuarioModificacion(), PDO::PARAM_STR);
                if($stmt->execute()){
                    return "OK";
                }
                return $stmt->errorInfo()[2];
            } catch (PDOException $error) {
                return $error->error_reporting();
            }
        }

        public function ModificarMunicipio($municipio){
            try {
                $sql = "UPDATE `ubicacion_municipio`
                SET `idDepartamento` = :idDepartamento,
                  `codigoDane` = :codigoDane,
                  `descripcion` = :descripcion,
                  `estado` = :estado,
                  `fechaModificacion` = (SELECT NOW()),
                  `idUsuarioModificacion` = :idUsuarioModificacion
                WHERE `idMunicipio` = :idMunicipio;";

                $conexion = new Conexion();
                $stmt = $conexion->prepare($sql);

                $stmt->bindValue(":idDepartamento", $municipio->getIdDepartamento(), PDO::PARAM_STR);
                $stmt->bindValue(":codigoDane", $municipio->getCodigoDane(), PDO::PARAM_STR);
                $stmt->bindValue(":descripcion", $municipio->getDescripcion(), PDO::PARAM_STR);
                $stmt->bindValue(":estado", $municipio->getEstado(), PDO::PARAM_INT);
                $stmt->bindValue(":idMunicipio", $municipio->getIdMunicipio(), PDO::PARAM_STR);
                $stmt->bindValue(":idUsuarioModificacion", $municipio->getIdUsuarioModificacion(), PDO::PARAM_STR);
                if($stmt->execute()){
                    return "OK";
                }
                return $stmt->errorInfo()[2];
            } catch (PDOException $error) {
                return $error->error_reporting();
            }
        }

        public function EliminarMunicipio($idMunicipio){
            try {
                $sql = "DELETE FROM ubicacion_municipio WHERE idMunicipio = :idMunicipio";
                $conexion = new Conexion();
                $stmt = $conexion->prepare($sql);
                $stmt->bindValue(":idMunicipio", $municipio->getIdMunicipio(), PDO::PARAM_STR);
                if($stmt->execute()){
                    return "OK";
                }
                return $stmt->errorInfo()[2];
            } catch (PDOException $error) {
                return $error->error_reporting();
            }
        }
    }

?>