<script src="<?php echo $url; ?>/js/documento/documento_guardar.js"></script>
<div class="content-wrapper">
    <section class="content-header">
      <h1>
        Tablero
        <small>Panel de control</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Inicio</a></li>
        <li><a href="active">Tablero</a></li>
      </ol>
    </section>

    <section class="content">
      <form method="POST" id="form">
          <h4 class="box-title">Documentos</h4>
          <div class="box box-primary">
              <div class="box-header with-border">
                <button class="btn btn-primary guardar" type="submit"><i class="fa fa-save"></i> Guardar</button>
              </div>
              <div class="box-body">

                <div class="row">
                    <div class="form-group col-md-6">
                        <label for="ddlEntidad" class="control-label">Entidad</label>
                        <select name="ddlEntidad" id="ddlEntidad" class="form-control">
                        </select>
                    </div>
                    <div class="form-group col-md-6">
                        <label for="ddlTipoDocumento" class="control-label">Tipo Documento</label>
                        <select name="ddlTipoDocumento" id="ddlTipoDocumento" class="form-control">
                        </select>
                    </div>
                </div>

                <div class="row">
                    <div class="form-group col-md-6">
                        <label for="ddlDepartamento" class="control-label">Departamento</label>
                        <select name="ddlDepartamento" id="ddlDepartamento" class="form-control" onChange="ConsultaMunicipio();">
                        </select>
                    </div>
                    <div class="form-group col-md-6">
                        <label for="ddlMunicipio" class="control-label">Municipio</label>
                        <select name="ddlMunicipio" id="ddlMunicipio" class="form-control">
                        </select>
                    </div>
                </div>

                <div class="row">
                    <div class="form-group col-md-6">
                        <label for="txtFecha" class="control-label">Fecha</label>
                        <input type="text" name="txtFecha" id="txtFecha" class="form-control" required>
                    </div>
                    <div class="form-group col-md-6">
                        <label for="fileDocumento" class="control-label">Documento</label>
                        <input type="file" name="fileDocumento" id="fileDocumento" required>
                        <h6><p class="help-block">Selecciona archivos tipo (PDF)</p></h6>
                        <h6><p class="help-block">Tamaño máximo del archivo 1GB</p></h6>
                    </div>
                </div>

                <div class="box box-info collapsed-box">
                  <div class="box-header with-border">

                    <div class="box-title">Objetos</div>
                    <div class="box-tools pull-right">
                      <button class="btn btn-box-tool" type="button" data-widget="collapse">
                        <i class="fa fa-plus"></i>
                      </button>
                    </div>

                  </div>
                  <div class="box-body">

                    <div class="form-horizontal">
                      <div class="form-group">
                        <label for="txtObjeto" class="label-control col-md-1">Objeto:</label>
                        <div class="col-md-4">
                          <input type="text" name="txtObjeto" id="txtObjeto" class="form-control">
                        </div>
                        <div class="col-md-2">
                          <button type="button" class="btn btn-success botonObjetos"><i class="fa fa-plus"></i> Agregar Objeto</button>
                        </div>
                      </div>
                    </div>

                    <div class="col-md-6">
                      <table id="datosObjetos" class="table table-striped dt-responsive">
                        <thead>
                          <tr>
                            <th>Objeto</th>
                            <th>Acciones</th>
                          </tr>
                        </thead>
                        <tbody>
                        </tbody>
                      </table>
                    </div>

                  </div>
                </div>

                <div class="box box-warning collapsed-box">
                  <div class="box-header with-border">

                    <div class="box-title">Intervinentes</div>
                    <div class="box-tools pull-right">
                      <button class="btn btn-box-tool" type="button" data-widget="collapse">
                        <i class="fa fa-plus"></i>
                      </button>
                    </div>

                  </div>
                  <div class="box-body">
                    <div class="form-horizontal">

                      <div class="form-group">
                        <label for="txtIntervinente" class="label-control col-md-1">Intervinente:</label>
                        <div class="col-md-4">
                          <input type="text" name="txtIntervinente" id="txtIntervinente" class="form-control">
                        </div>
                        <div class="col-md-2">
                          <button type="button" class="btn btn-success botonIntervinente"><i class="fa fa-plus"></i> Agregar Intervinente</button>
                        </div>
                      </div>

                    </div>

                    <div class="col-md-6">
                      <table id="datosIntervinente" class="table table-striped dt-responsive">
                        <thead>
                          <tr>
                            <th>Intervinente</th>
                            <th>Acciones</th>
                          </tr>
                        </thead>
                        <tbody>
                        </tbody>
                      </table>
                    </div>

                  </div>
                </div>

                <div class="box box-success collapsed-box">
                  <div class="box-header with-border">

                    <div class="box-title">Información adicional</div>
                    <div class="box-tools pull-right">
                      <button class="btn btn-box-tool" type="button" data-widget="collapse">
                        <i class="fa fa-plus"></i>
                      </button>
                    </div>

                  </div>
                  <div class="box-body">

                    <div class="row">
                      <div class="col-md-4">
                        <label for="ddlInformacionAdicional" class="label-control">Información adicional:</label>
                        <select name="ddlInformacionAdicional" id="ddlInformacionAdicional" class="form-control">
                        </select>
                      </div>
                      <div class="col-md-4">
                        <label for="txtValor" class="label-control">Valor:</label>
                        <input type="text" name="txtValor" id="txtValor" class="form-control">
                      </div>
                      <div class="col-md-2">
                        <button type="button" class="btn btn-success botonInformacionAdicional"><i class="fa fa-plus"></i> Agregar Información Adicional</button>
                      </div>
                    </div>

                    <div class="col-md-6">
                      <table id="datosInformacionAdicional" class="table table-striped dt-responsive">
                        <thead>
                          <tr>
                            <th>#</th>
                            <th>Información Adicional</th>
                            <th>Valor</th>
                            <th>Acciones</th>
                          </tr>
                        </thead>
                        <tbody>
                        </tbody>
                      </table>
                    </div>

                  </div>
                </div>

              </div>
              <div class="box-footer">
                <button class="btn btn-primary guardar" type="submit"><i class="fa fa-save"></i> Guardar</button>
              </div>
          </div>
        </form>
    </section>

  </div>