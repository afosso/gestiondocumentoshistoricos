

<?php

    require_once str_replace("\\", "/", dirname(__DIR__,2)) . '/entorno/conexion.php';
    $idDocumento = $_GET["parametro"]; 
    $resultado;
    if(!filter_var($idDocumento, FILTER_VALIDATE_INT)){
        die("No juegues con los valores");
    }else{
        $sql = "SELECT * FROM documento_documento WHERE idDocumento = $idDocumento";
        $conexion = new Conexion();
        $stmt = $conexion->prepare($sql);
        $stmt->execute();
        $resultado = $stmt->fetch(\PDO::FETCH_OBJ);
    }

?>

<div class="content-wrapper">
    <section class="content-header">
      <h1>
        Tablero
        <small>Panel de control</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Inicio</a></li>
        <li><a href="active">Tablero</a></li>
      </ol>
    </section>

    <section class="content">
    <?php if($resultado == null){die("<div class='alert alert-info'>No existen datos con el valor asignado<div>");} ?>
      <form method="POST" id="form">
          <h4 class="box-title">Visualizador de archivos</h4>
          <div class="box box-primary">
              <div class="box-header with-border">
                <h2 class="header"><?php echo "Visualizando " . $resultado->nombreArchivo; ?></h2>
              </div>
              <div class="box-body">
                <div class="form-horizontal">
                  
                <?php
                    /*$decoded = base64_decode($resultado->rutaArchivo);
                    $file = 'invoice.pdf';
                    file_put_contents($file, $decoded);

                    if (file_exists($file)) {
                        header('Content-Description: File Transfer');
                        header('Content-Type: application/octet-stream');
                        header('Content-Disposition: attachment; filename="'.basename($file).'"');
                        header('Expires: 0');
                        header('Cache-Control: must-revalidate');
                        header('Pragma: public');
                        header('Content-Length: ' . filesize($file));
                        readfile($file);
                        exit;
                    }*/
                    echo '<div class="embed-responsive embed-responsive-16by9">';
                    echo "<object class='embed-responsive-item' data='" . $resultado->rutaArchivo . "' allowfullscreen>";
                    echo '</div>';
                ?>

                </div>
              </div>
          </div>
        </form>
    </section>

  </div>