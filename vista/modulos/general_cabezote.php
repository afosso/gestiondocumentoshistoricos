<header class="main-header">

    <!--
        =================
        LOGOTIPO
        =================
    -->
    <a href="" class="logo">
        <!-- LOGO MINI -->
        <span class="logo-mini">
            <figure>
                <img src="./vista/dist/img/Doc.png" alt="Gestión Documental Academia" height="40" >
            </figure>
        </span>
        <!-- LOGO NORMAL -->
        <span class="logo-lg">
            <figure>
                <img src="./vista/dist/img/Doc.png" alt="Gestión Documental Academia" height="40" >
            </figure>
        </span>
    </a>
    <!--
        =================
        BARRA NAVEGACION
        =================
    -->
    <nav class="navbar navbar-static-top" role="navigation">
        <!-- Botón de navegación -->
        <ul class="nav navbar-nav">
            <li class="nav-item">
                <a href="#" class="nav-link" data-toggle="push-menu">
                    <i class="fa fa-bars"></i>
                </a>
            </li>
        </ul>
        <!-- Perfil de usuario -->
        <div class="navbar-custom-menu">
            <ul class="nav navbar-nav">
                <li class="dropdown user user-menu">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                        <img src="<?php  echo $_SESSION["fotoUser"]; ?>" class="user-image" alt="User Image">
                        <span class="hidden-xs"><?php echo $_SESSION["nombreUsuario"]; ?></span>
                    </a>
                    <!-- Dropdown Perfil de Usuario -->
                    <ul class="dropdown-menu">
                        <li class="user-body">
                            <div class="pull-right">
                                <a href="<?php echo $url; ?>/general_salir"><span class="fa fa-times"></span> Cerrar sesión</a>
                            </div>
                        </li>
                    </ul>
                </li>
            </ul>
        </div>
    </nav>

</header>