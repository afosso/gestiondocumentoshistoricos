<script src="<?php echo $url; ?>/js/documento/documento_consulta_publica.js"></script>
    <form method="POST" id="form">
        <h4 class="box-title">Consulta</h4>
        <div class="box box-primary">

            <div class="box-header with-border">
            <button class="btn btn-primary consultar" type="submit"><i class="fa fa-search"></i> Consultar</button><br>
            <span class="text-help">Recuerda que puedes digitar cualquier opción para la búsqueda</span>

            <div class="row">

                <div class="form-group col-md-6">
                <label for="txtNombreDocumento" class="control-label">Nombre documento</label>
                <input type="text" name="txtNombreDocumento" id="txtNombreDocumento" class="form-control" placeholder="Digite cualquier palabra del documento">
                </div>

                <div class="form-group col-md-6">
                <label for="txtObjeto" class="control-label">Objeto</label>
                <input type="text" name="txtObjeto" id="txtObjeto" class="form-control" placeholder="Digite cualquier palabra del objeto">
                </div>
                
            </div>

            <div class="row">

                <div class="form-group col-md-6">
                <label for="txtInterviniente" class="control-label">interviniente</label>
                <input type="text" name="txtInterviniente" id="txtInterviniente" class="form-control" placeholder="Digite un interviniente">
                </div>
                
            </div>

            </div>
            <div class="box-body">
                <table id="datos" class="table table-bordered table-striped dt-responsive tabla">
                </table>
            </div>
        </div>
    </form>