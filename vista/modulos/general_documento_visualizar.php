<div class="container-fluid">

<?php

require_once str_replace("\\", "/", dirname(__DIR__,2)) . '/entorno/conexion.php';
$idDocumento = $_GET["parametro"]; 
$resultado;
if(!filter_var($idDocumento, FILTER_VALIDATE_INT)){
    die("No juegues con los valores");
}else{
    $sql = "SELECT * FROM documento_documento WHERE idDocumento = $idDocumento";
    $conexion = new Conexion();
    $stmt = $conexion->prepare($sql);
    $stmt->execute();
    $resultado = $stmt->fetch(\PDO::FETCH_OBJ);
}

?>

<?php if($resultado == null){die("<div class='alert alert-info'>No existen datos con el valor asignado<div>");} ?>
  <form method="POST" id="form">
      <h4 class="box-title">Visualizador de archivos</h4>
      <div class="box box-primary">
          <div class="box-header with-border">
            <h2 class="header"><?php echo "Visualizando " . $resultado->nombreArchivo; ?></h2>
          </div>
          <div class="box-body">
            <div class="form-horizontal">
              
            <?php
                echo '<div class="embed-responsive embed-responsive-16by9">';
                echo "<object class='embed-responsive-item' data='" . $resultado->rutaArchivo . "' allowfullscreen>";
                echo '</div>';
            ?>

            </div>
          </div>
      </div>
    </form>

</div>