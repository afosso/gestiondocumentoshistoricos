<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Tablero
        <small>Panel de control</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Inicio</a></li>
        <li><a href="active">Tablero</a></li>
      </ol>
    </section>

    <section class="content">
        <div class="row">
            <div class="col-lg-4 col-xs-6">
                <div class="small-box bg-green">
                    <div class="inner">
                        <h3>
                        <?php
                            require_once str_replace("\\", "/", dirname(__DIR__,2)) . '/entorno/conexion.php';
                            $sql = "SELECT COUNT(idDocumento) AS 'cantidadDocumento' FROM documento_documento";
                            $conexion = new Conexion();
                            $stmt = $conexion->prepare($sql);
                            $stmt->execute();
                            $resultado = $stmt->fetch(\PDO::FETCH_OBJ);
                            echo $resultado->cantidadDocumento;
                        ?>
                        </h3>
                        <p>Documentos subidos</p>
                    </div>
                    <div class="icon">
                        <i class="fa fa-file"></i>
                    </div>
                </div>
            </div>
            <div class="col-lg-4 col-xs-6">
                <div class="small-box bg-yellow">
                    <div class="inner">
                        <h3><?php
                            require_once str_replace("\\", "/", dirname(__DIR__,2)) . '/entorno/conexion.php';
                            $sql = "SELECT COUNT(idDocumento) AS 'cantidadDocumento' FROM documento_documento WHERE DATE(fechaCreacion) = DATE((SELECT NOW()))";
                            $conexion = new Conexion();
                            $stmt = $conexion->prepare($sql);
                            $stmt->execute();
                            $resultado = $stmt->fetch(\PDO::FETCH_OBJ);
                            echo $resultado->cantidadDocumento;
                        ?></h3>
                        <p>Documentos subidos hoy</p>
                    </div>
                    <div class="icon">
                        <i class="fa fa-file-signature"></i>
                    </div>
                </div>
            </div>
            
        </div>
    </section>

  </div>