<?php 
    require_once str_replace("\\", "/", dirname(__DIR__,2)) . '/entorno/conexion.php';

    $idCarpeta = $_GET["parametro"]; 
    $resultado;
    $resultadoCarpeta;
    if(!filter_var($idCarpeta, FILTER_VALIDATE_INT)){
        die("No juegues con los valores");
    }else{
        $sql = "SELECT * FROM seguridad_carpeta WHERE idCarpeta = $idCarpeta";
        $conexion = new Conexion();
        $stmt = $conexion->prepare($sql);
        $stmt->execute();
        $resultado = $stmt->fetch(\PDO::FETCH_OBJ);

        $stmt = $conexion->prepare("SELECT * FROM seguridad_carpeta");
        $stmt->execute();
        $resultadoCarpeta = $stmt->fetchAll(PDO::FETCH_OBJ);
    }

?>

<script src="<?php echo $url; ?>/js/seguridad/carpeta_modifica.js"></script>
<div class="content-wrapper">
    <section class="content-header">
      <h1>
        Tablero
        <small>Panel de control</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Inicio</a></li>
        <li><a href="active">Tablero</a></li>
      </ol>
    </section>

    <section class="content">
      <?php if($resultado == null){die("<div class='alert alert-info'>No existen datos con el valor asignado<div>");} ?>

      <form method="POST" id="form">
          <h4 class="box-title">Carpetas</h4>
          <div class="box box-primary">
              <div class="box-header with-border">
                <button class="btn btn-primary modificar" type="submit"><i class="fa fa-edit"></i> Modificar</button>
              </div>
              <div class="box-body">
                <div class="form-horizontal">
                  <input type="hidden" name="hiddenIdCarpeta" id="hiddenIdCarpeta" value="<?php echo $resultado->idCarpeta; ?>">

                  <div class="form-group">
                    <label for="ddlCarpetaPadre" class="control-label col-sm-2">Carpeta padre</label>
                    <div class="col-sm-4">
                      <select type="text" name="ddlCarpetaPadre" id="ddlCarpetaPadre" class="form-control">
                        <option value="-1">--Seleccione una opción--</option>
                        <?php
                          if($resultadoCarpeta != null){
                            foreach ($resultadoCarpeta as $key => $value) {
                              $seleccionado = "";
                              if($resultado->idCarpetaPadre == $value->idCarpeta){
                                $seleccionado = 'selected=""';
                              }else{
                                $seleccionado = "";
                              }
                              echo '<option value="' . $value->idCarpeta . '" ' . $seleccionado . '>' . $value->descripcion . '</option>';
                            }
                          }
                        ?>
                      </select>
                    </div>
                  </div>

                  <div class="form-group">
                    <label for="txtCodigo" class="control-label col-sm-2">Código</label>
                    <div class="col-sm-4">
                      <input type="text" name="txtCodigo" id="txtCodigo" class="form-control" value="<?php echo $resultado->codigo; ?>">
                    </div>
                  </div>

                  <div class="form-group">
                    <label for="txtDescripcion" class="control-label col-sm-2">Descripción</label>
                    <div class="col-sm-4">
                      <input type="text" name="txtDescripcion" id="txtDescripcion" class="form-control" value="<?php echo $resultado->descripcion; ?>">
                    </div>
                  </div>

                  <div class="form-group">
                    <label for="ddlEstado" class="control-label col-sm-2">Estado</label>
                    <div class="col-sm-4">
                      <select type="text" name="ddlEstado" id="ddlEstado" class="form-control">
                        <option value="1" <?php if($resultado->estado == '1'){echo 'selected=""';} ?>>Activo</option>
                        <option value="0" <?php if($resultado->estado == '0'){echo 'selected=""';} ?>>Inactivo</option>
                      </select>
                    </div>
                  </div>

                </div>
              </div>
              <div class="box-footer">
                <button class="btn btn-primary modificar" type="submit"><i class="fa fa-edit"></i> Modificar</button>
              </div>
          </div>
        </form>
    </section>

  </div>