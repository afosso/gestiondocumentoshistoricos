<?php 
    require_once str_replace("\\", "/", dirname(__DIR__,2)) . '/entorno/conexion.php';
    $idUsuario = $_GET["parametro"]; 
    $resultado;
    if(!filter_var($idUsuario, FILTER_VALIDATE_INT)){
        die("No juegues con los valores");
    }else{
        $sql = "SELECT * FROM seguridad_usuario WHERE idUsuario = $idUsuario";
        $conexion = new Conexion();
        $stmt = $conexion->prepare($sql);
        $stmt->execute();
        $resultado = $stmt->fetch(\PDO::FETCH_OBJ);
    }

?>

<script src="<?php echo $url; ?>/js/seguridad/usuario_modifica.js"></script>

<div class="content-wrapper">
    <section class="content-header">
      <h1>
        Tablero
        <small>Panel de control</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Inicio</a></li>
        <li><a href="active">Tablero</a></li>
      </ol>
    </section>

    <section class="content">
        <?php if($resultado == null){die("<div class='alert alert-info'>No existen datos con el valor asignado<div>");} ?>

        <form method="POST" id="form">
          <h4 class="box-title">Modificar Usuarios</h4>
          <div class="box box-primary">
              <div class="box-header with-border">
                <button class="btn btn-primary modificar" type="submit"><i class="fa fa-edit"></i> Modificar</button>
              </div>
              <div class="box-body">

                <input type="hidden" name="hiddenFoto" id="hiddenFoto" value="<?php echo $resultado->fotoUrl; ?>">
                <input type="hidden" name="hiddenIdUsuario" id="hiddenIdUsuario" value="<?php echo $resultado->idUsuario; ?>">
                <div class="form-horizontal">
                  
                    <div class="form-group">
                        <div class="col-sm-6 col-sm-offset-2">
                        <img id="imagen" src="<?php echo $resultado->fotoUrl; ?>" alt="" class="img-circle" style="width:150px;height:150px;">
                        </div>
                    </div>

                    <div class="form-group">
                        <div class="col-sm-6 col-sm-offset-2">
                            <input type="file" id="inputFotos" name="inputFotos" required>
                            <h6><p class="help-block">Selecciona imágenes tipo (JPG, PNG, JPEG)</p></h6>
                            <h6><p class="help-block">Tamaño máximo de la imágen 30MB</p></h6>
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="txtCodigo" class="control-label col-sm-2">Nombre de usuario</label>
                        <div class="col-sm-4">
                        <input type="text" name="txtCodigo" id="txtCodigo" class="form-control" placeholder="Nombre de usuario" maxlength="20" required value="<?php echo $resultado->codigo; ?>">
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="txtDescripcion" class="control-label col-sm-2">Descripción del usuario</label>
                        <div class="col-sm-4">
                        <input type="text" name="txtDescripcion" id="txtDescripcion" class="form-control" placeholder="Descripción del usuario" maxlength="250" required value="<?php echo $resultado->descripcion; ?>">
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="txtContrasenia" class="control-label col-sm-2">Contraseña</label>
                        <div class="col-sm-4">
                        <input type="password" name="txtContrasenia" id="txtContrasenia" class="form-control" placeholder="Contraseña" maxlength="250" required>
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="txtFechaActivacion" class="control-label col-sm-2">Fecha de activación</label>
                        <div class="col-sm-4">
                        <input type="text" name="txtFechaActivacion" id="txtFechaActivacion" class="form-control" placeholder="yyyy-mm-dd" required value="<?php echo $resultado->fechaActivacion; ?>">
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="txtFechaExpiracion" class="control-label col-sm-2">Fecha de expiración</label>
                        <div class="col-sm-4">
                        <input type="text" name="txtFechaExpiracion" id="txtFechaExpiracion" class="form-control" placeholder="yyyy-mm-dd" required value="<?php echo $resultado->fechaExpiracion; ?>">
                        </div>
                    </div>

                    <div class="form-group">
                        <div class="col-sm-offset-2 col-sm-10">
                        <div class="checkbox">
                            <label>
                            <input type="checkbox" name="chkAdministrador" id="chkAdministrador" checked="<?php echo ($resultado->administrador == 1 ? "true":"false"); ?>">
                            Administrador
                            </label>
                        </div>
                        </div>
                    </div>

                </div>
              </div>
              <div class="box-footer">
                <button class="btn btn-primary modificar" type="submit"><i class="fa fa-edit"></i> Modificar</button>
              </div>
          </div>
        </form>
    </section>

  </div>